import Data.List
import Data.Function (on, fix)
import Data.Maybe
import Data.Array
import Control.Monad
import Control.Applicative
import Control.Arrow
import Data.Map ((!), fromList, fromListWith, adjust, keys, Map)
import Debug.Trace

graph = [('a','c',2), ('a','d',6), ('b','a',3)
        ,('b','d',8), ('c','d',7), ('c','e',5)
        ,('d','e',10), ('e','b',1)]

graph3 = [ ('a','b',4),('a','c',6),('a','d',1),('a','e',7)
        , ('b','c',3),('b','d',8),('b','e',4)
        , ('c','d',9),('c','e',3)
        , ('d','e',2)]

graph2 = [('a','b',7),('a','c',9),('a','f',14)
        ,('b','a',7),('b','c',10),('b','d',15)
        ,('c','a',9),('c','b',10),('c','d',11),('c','f',1)
        ,('d','b',15),('d','c',11),('d','e',6)
        ,('e','d',6),('e','f',9)
        ,('f','a',14),('f','c',1),('f','e',9)]

s = [["a"],[],[],[],[]]
s' = [[("a",0.0::Float)],[],[],[],[]]
s'' = ["a"]

assocl :: (a,(b,c)) -> ((a,b),c)
assocl (a,(b,c)) = ((a,b),c)

assocr :: ((a,b),c) -> (a,(b,c))
assocr ((a,b),c) = (a,(b,c))

----------------------------------------
--

isEdge :: (Char, Char) -> Bool
isEdge (s,t) = any (\(ss,tt,_) -> (s == ss) && (t == tt)) graph

getVertices :: [(Char, Char, Float)] -> [Char]
getVertices = nub . uncurry (++) . unzip . map (\(s,t,_) -> (s,t))

canextend :: (Char, [Char]) -> Maybe (Char, [Char])
canextend (e,[]) = Nothing
canextend (e,p@(x:xs)) = if isEdge (x,e)
                         then Just (e,p)
                         else Nothing

notinlist :: (Char, [Char]) -> Maybe (Char, [Char])
notinlist (e,p) = if elem e p then Nothing else Just (e,p)

mcons :: (a, [a]) -> Maybe [a]
mcons = return . (uncurry (:))

fromto :: Char -> Char -> [Char] -> Maybe [Char]
fromto s t p = if (True,True) == (((==s).head) &&& ((==t).last)) p then Just p else Nothing

extend :: Char -> [Char] -> Maybe [Char]
extend = curry (notinlist >=> canextend >=> mcons)

----------------------------------------
--

edgeLength :: (Char, Char) -> Float
edgeLength (s,t) = case find (\(ss,tt,_) -> (s == ss) && (t == tt)) graph of
  Just (_,_,d) -> d
  Nothing      -> 0

pathLengths :: [[Char]] -> [([Char], Float)]
pathLengths = map (ap (,) pathLength)

pathLength :: [Char] -> Float
pathLength = sum . map edgeLength . liftA2 zip tail id -- zip <$> tail <*> id

----------------------------------------
--

cosnd :: Ord b => (a,b) -> (a,b) -> Ordering
cosnd = compare `on` snd

mcosnd :: Ord b => [(a,b)] -> (a,b)
mcosnd = minimumBy cosnd

mcosnd' :: Ord b => [(a,b)] -> [(a,b)]
mcosnd' [] = []
mcosnd' x = [minimumBy cosnd x]

shortestPath = map fst . mcosnd' . pathLengths

----------------------------------------
--

-- step :: [[[Char]]] -> [[[Char]]]
-- step paths = map catMaybes $ [map (extend x) (concat paths) | x <- "abcde"]

-- extend one path
explore :: [Char] -> [[Char]]
explore path = catMaybes [extend x path | x <- getVertices graph]

step   :: [[Char]] -> ([[Char]], [[Char]])
step   = (explore *** uncurry (++)) . assocr . ((id &&& explore) *** id) . select

step'' :: [[Char]] -> ([[Char]], [[Char]])
step'' = (explore *** uncurry (++)) . assocr . ((id &&& explore) *** id) . select''

-- (id *** filterShortest) .

select :: [[Char]] -> ([Char], [[Char]])
select (x:xs) = (x,xs)

select' :: [[Char]] -> ([Char], [[Char]])
select' paths = (n, delete n paths)
  where
    n = fst . minimumBy cosnd . pathLengths $ paths

select'' :: [[Char]] -> ([Char], [[Char]])
select'' paths = (n, np)
  where
    n = fst . minimumBy cosnd . pathLengths $ paths
    np = delete n . filterShortest $ paths

filterShortest paths = concat . map shortestPath $ [(filter ((==x).head)) paths | x <- getVertices graph]

build = concat . unfoldr (\x -> if null x then Nothing else Just . step $ x) $ ["a"]

----------------------------------------
-- pomocou katamorfizmu?

concatFilterShortest = (filterShortest.) . (++)

foldstep :: Char -> ([[Char]],[[Char]]) -> ([[Char]],[[Char]])
foldstep x (all, next) = (all ++ new, new)
  where new = concat . foldr (\s a -> (:a) . fst . step $ [s]) [] $ next

foldstep' :: Char -> ([[Char]],[[Char]]) -> ([[Char]],[[Char]])
foldstep' x (all, next) = (concatFilterShortest all new, new)
  where new = foldr (\s a -> concatFilterShortest a . fst . step $ [s]) [] $ next

foldstep'' :: Char -> ([[Char]],[[Char]]) -> ([[Char]],[[Char]])
foldstep'' x (all, next) = (concatFilterShortest all *** union (next \\ [sel])) new
  where
    sel = let x = fst . select'' $ next in {-trace ("\nSEL IS " ++ show x)-} x
    new = let x = step'' next in {-trace ("\nNEW IS ++ " ++ show x)-} x

flood _ it = nub . (++) it . concat . foldr (\s a -> (:a) . fst . step $ [s]) [] $ it
flood' _ it = ("b":) . concat . foldr (\s a -> (:a) . fst . step $ [s]) [] $ it
allPaths = foldr flood ["b"] $ getVertices graph

floodMap = concat . map (\s -> fst . step $ [s])
floodMap' = foldr concatFilterShortest [] . map (\s -> fst . step $ [s])
floods = foldr (\_ ac -> ac ++ floodMap ac)

floodFilter _ it = concatFilterShortest it . concat . foldr (\s a -> (:a) . fst . step $ [s]) [] $ it
allPathsFilter = foldr floodFilter ["b"] $ getVertices graph
allPathsScanr = scanr floodFilter ["b"] $ getVertices graph

----------------------------------------
-- limity! yay
