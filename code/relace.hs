{-# LANGUAGE NoMonomorphismRestriction #-}

import Data.List

subseq :: [a] -> [[a]]
subseq = foldr (\a xs -> [a:x | x <- xs] ++ xs) [[]]

cplist :: [[a]] -> [[a]]
cplist = foldr (\xs ys -> [(a:b) | a <- xs, b <- ys]) [[]]

inits :: [a] -> [[a]]
inits = foldr (\a xs -> [] : [a:x | x <- xs]) [[]]

-- [a:x | x <- xs] == map (a:) xs

pref :: (a -> Bool) -> [a] -> [[a]]
pref p = foldr (\a xs -> [] : [a:x | x <- xs, p a]) [[]]

takewh :: Ord a => (a -> Bool) -> [a] -> [[a]]
takewh p = foldr (\a xs -> [maximum ([] : [a:x | x <- xs, p a])]) [[]]

takewh' p = foldr (\a xs -> if p a then a:xs else []) []

mss' = maximum . map (foldr (\a s -> maximum [0, a+s]) 0) . tails
-- tupple transform
mss = snd . foldr (\a (prf,seg) -> let x = 0 `max` (a+prf) in (x, x `max` seg)) (0,0)

partition :: [a] -> [[[a]]]
partition = foldr (\a xs -> [[a]:y | y <- xs] ++ [(a:x):y | (x:y) <- xs]) [[]]
-- alebo pomocou dup

partition' = foldr (\a xs -> concat . map (\(a,xs) -> [[a]:xs,(a:head xs):tail xs]) $ [(a,y) | y <- xs]) [[]]

dup f x = f x x
pair f g a = (f a, g a)

interleaveC :: [a] -> [([a],[a])]
interleaveC = foldr (\a xs -> [consl a p | p <- xs] ++ [consr a p | p <- xs]) [([],[])]

interleave :: ([a],[a]) -> [[a]]
interleave = foldr (\(x, y) as -> [ x:a | a <- as] ++ [ y:a | a <- as]) [[]] . uncurry zip

consl a (xs, ys) = (a:xs, ys)
consr a (xs, ys) = (xs, a:ys)


data BT a = Tip a | Bin (BT a) (BT a) deriving (Show)

instance Functor BT where
  fmap f (Tip a) = Tip (f a)
  fmap f (Bin x y) = Bin (fmap f x) (fmap f y)

binfold :: (a -> b) -> (b -> b -> b) -> BT a -> b
binfold g h (Tip a) = g a
binfold g h (Bin x y) = h (binfold g h x) (binfold g h y)

cptree :: BT [a] -> [BT a]
cptree = binfold
         (\xs -> [Tip a | a <- xs])
         (\xs ys -> [Bin a b | a <- xs, b <- ys])

bt :: BT Int
bt = Bin (Bin (Bin (Tip 1) (Tip 2)) (Bin (Tip 3) (Tip 4))) (Bin (Tip 5) (Tip 6))

intree = binfold (:[]) (\xs ys -> xs ++ ys)

pow :: Integer -> Int -> Integer
pow x n = (!!(n-1)) $ iterate (*x) x

digits :: Integer -> [Integer]
digits m = if m < 10 then [m] else (digits (m `div` 10)) ++ [m `mod` 10]

digits' :: Integer -> [Integer] -> [Integer]
digits' m x = if m < 10 then [m] ++ x else digits' (m `div` 10) ([m `mod` 10] ++ x)

shift :: Integer -> Integer -> Integer -> Integer
shift b n d = n*b + d

bin :: [Integer] -> Integer
bin = foldr (flip (shift 2)) 0

data ListN a = Wrap a | Cons a (ListN a) deriving Show

foldr' :: (a -> b) -> (a -> b -> b) -> ListN a -> b
foldr' f g (Wrap a) = f a
foldr' f g (Cons a x) = g a (foldr' f g x)

inlist = foldr' (:[]) (\a xs -> nub ([ y | y <- xs] ++ [ a | y <- xs]))

ordered :: Ord a => [a] -> [[a]]
ordered = foldr (\a xs -> [(a:x) | x <- xs, ok (a,x)]) [[]]
  where ok (a,x) = all (a<) x

qsort [] = []
qsort x = let (y,a,z) = split x in qsort y ++ qsort z ++ [a]

split [b] = ([],b,[])
split (u:us) = ([ q | q <- us, q < u], u, [ q | q <- us, q >= u])
