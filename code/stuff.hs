import Data.List

begin :: ([Int] -> b) -> b
push :: [Int] -> Int -> ([Int] -> b) -> b
add :: [Int] -> ([Int] -> b) -> b
end :: [b] -> b
begin x = x []
push s x f = f (x:s)
add (a:b:s) f = f (a+b:s)
end (x:[]) = x


unfold :: (a -> Bool) -> (a -> b) -> (a -> a) -> a -> [b]
unfold stop mapper next seed
  = map mapper $ takeWhile (not . stop) $ iterate next seed

data Tree a = Node a [Tree a]
            deriving (Show, Eq)

value :: Tree a -> a
value (Node a _) = a

children :: Tree a -> [Tree a]
children (Node _ cs) = cs

bfs :: (a -> Bool) -> Tree a -> [a]
bfs p t = filter p . concat . levels $ t

levels :: Tree a -> [[a]]
levels t = unfold null (map value) (concatMap children) [t]
