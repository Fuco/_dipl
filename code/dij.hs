import Data.List
import Data.Function (on, fix)
import Data.Maybe
import Control.Monad
import Control.Applicative
import Control.Arrow
import Data.Map ((!), fromList, fromListWith, adjust, keys, Map)
import Debug.Trace

graph = [('a','c',2), ('a','d',6), ('b','a',3)
        ,('b','d',8), ('c','d',1), ('c','e',5)
        ,('d','e',10), ('e','b',1)]

s = [["a"],[],[],[],[]]
s' = [[("a",0.0::Float)],[],[],[],[]]

s'' = ["a"]
----------------------------------------
--

isEdge :: (Char, Char) -> Bool
isEdge (s,t) = any (\(ss,tt,_) -> (s == ss) && (t == tt)) graph

canextend :: (Char, [Char]) -> Maybe (Char, [Char])
canextend (e,[]) = Nothing
canextend (e,p@(x:xs)) = if isEdge (x,e)
                         then Just (e,p)
                         else Nothing

notinlist :: (Char, [Char]) -> Maybe (Char, [Char])
notinlist (e,p) = if elem e p then Nothing else Just (e,p)

mcons :: (a, [a]) -> Maybe [a]
mcons = return . (uncurry (:))

fromto :: Char -> Char -> [Char] -> Maybe [Char]
fromto s t p = if (True,True) == (((==s).head) &&& ((==t).last)) p then Just p else Nothing

extend :: Char -> [Char] -> Maybe [Char]
extend = curry (notinlist >=> canextend >=> mcons)

----------------------------------------
--

edgeLength :: (Char, Char) -> Float
edgeLength (s,t) = case find (\(ss,tt,_) -> (s == ss) && (t == tt)) graph of
  Just (_,_,d) -> d
  Nothing      -> 0

pathLengths :: [[Char]] -> [([Char], Float)]
pathLengths = map (ap (,) pathLength)

pathLength :: [Char] -> Float
pathLength = sum . map edgeLength . liftA2 zip tail id -- zip <$> tail <*> id

----------------------------------------
--

cosnd :: Ord b => (a,b) -> (a,b) -> Ordering
cosnd = compare `on` snd

mcosnd :: Ord b => [(a,b)] -> (a,b)
mcosnd = minimumBy cosnd

mcosnd' :: Ord b => [(a,b)] -> [(a,b)]
mcosnd' [] = []
mcosnd' x = [minimumBy cosnd x]

shortestPath = map fst . mcosnd' . pathLengths

step :: [[[Char]]] -> [[[Char]]]
step paths = map catMaybes $ [map (extend x) (concat paths) | x <- "abcde"]

stepp paths = (shortest, concat . map catMaybes $ [map (extend x) [shortest] | x <- "abcde"])
  where
    shortest = fst . mcosnd . pathLengths $ paths

steppp :: [[[Char]]] -> ([Char], [[[Char]]])
steppp paths = (shortest, map catMaybes $ [map (extend x) [shortest] | x <- "abcde"])
  where
    shortest = fst . mcosnd . pathLengths . concat $ paths

anastep x = if null (concat x) then Nothing else Just (x, step x)
anastep' x = if null (concat x) then Nothing else Just (map pathLengths x, step x)
anastep'' x = if null (concat x)
            then Nothing
            else Just (x, map pathLengths (step (map (map fst) x)))

anastep''' x = if null (concat x) then Nothing else Just (x, (x ++ new) \\ [shortest])
  where
    (shortest, new) = stepp x

anastep'''' x = if null (concat x) then Nothing else Just (x, map shortestPath $ zipWith (++) x new)
  where
    (shortest, new) = steppp x

dij = concat . map pathLengths . expand''

expand :: ([[[Char]]], [[[Char]]]) -> ([[[Char]]], [[[Char]]])
expand (paths, from) = (zipWith (++) paths new, new)
  where
    new = step from
expand'' = map concat . transpose . unfoldr anastep
expand''' =  unfoldr anastep
