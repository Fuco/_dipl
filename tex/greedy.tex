\newpage
\chapter{Optimization Problems}
\label{sec:optim-probl}

In the remainder of this thesis we will concern ourselves with a class of problems where a set of solutions is generated and then the best one is chosen as the final result.  This usually means finding the cheapest, largest, smallest, etc. member in a set of possible solutions satisfying given constraints.  These problems form the class of \emph{Optimization problems}.  The various approaches will vary in the method that is used to generate the solutions.  The general way to specify an optimization problem is the form
\[
min\, R \cdot \Lambda G,
\]
where $R$ is a preorder comparing the solutions and $G$ is relation used to generate them.

Specifying programming problems as optimization ones is convenient because the paradigm is applicable to wide variety of problems, the specification is usually short and concise and there are several well-known strategies for solving them.

In this chapter we will present two approaches from~\cite{BirddeMoor96:Algebra}, namely \emph{Greedy algorithms} and \emph{Dynamic programming} and illustrate their use on selected problems.  In chapter~\ref{sec:limits} we will present a more general paradigm of \emph{Limits} based on work of~\cite{Curtis96arelational}.

The sub-class of problems discussed in this chapter can be specified in the form
\[
min\, R \cdot \Lambda (\cata{S} \cdot \cata{T}^\circ),
\]
that is taking minimum element with respect to $R$ in the set of solutions returned by a hylomorphism.

We will not focus on developing the theory for the most general case, rather we will demonstrate the theoretical results from~\cite{BirddeMoor96:Algebra} on concrete examples, one for each approach.

\section{Greedy algorithms}

We will look at the restricted class of problems where the relation $T$ is the initial algebra of the intermediate type of the hylomorphism.  This reduces the problem specification to $min\, R \cdot \Lambda \cata{S}$.

\begin{definition}\index{monotonic algebra}
An $\FF$-algebra $S : A \leftarrow \FF A$ is \emph{monotonic} on a relation $R : A \leftarrow A$ if
\[
S \cdot \FF R \subseteq R \cdot S.
\]
\end{definition}

\begin{example}
Consider function $cup : \PF A \leftarrow \PF A \times \PF A,$ a union of two sets.  This function is monotonic on $subset$, which we can express as inequality
\[
cup \cdot (subset \times subset) \subseteq subset \cdot cup.
\]
\end{example}

The following theorem uses the notion of monotonicity to ensure that picking the best partial solution at each step will lead to the best final solution.

\begin{theorem}[Greedy theorem, \cite{BirddeMoor96:Algebra}]\label{th:greedy}\index{Greedy theorem}
If $S$ is monotonic on a preorder $R^\circ$, then
\[
\cata{min\, R \cdot \Lambda S} \subseteq min\, R \cdot \Lambda \cata{S}.
\]
\end{theorem}

\begin{remark}
If the specification uses $max\, R$ instead of $min\, R$, the condition of monotonicity changes to $S$ being monotonic on $R$, not $R^\circ$.  This is because $max\, R = min\, R^\circ$.
\end{remark}

The condition of monotonicity can be translated into equivalent English statement: if partial solution $a$ is better than $b$ with respect to $R$ and $S$ is applied to $b$, then there exists a way of applying $S$ to $a$ such that this result will be a better partial solution than $S$ applied to $b$.

Yet another way to demonstrate this is the following diagram:
\[
\big(a \llongleftarrow {\FF R} b \llongrightarrow {S} x\big) \Rightarrow \big( a \llongrightarrow {S} y \llongleftarrow {R} x\big).
\]

\section{The activity list problem}

The following problem appears as an example in~\cite{Cormen:2001:IA:580470} in their chapter on greedy algorithms:

\begin{problemDescription}
  Suppose we have a set $S = \{1,2,\ldots,n\}$ of $n$ activities---for example lectures---that all need to use the same resource, such as a lecture hall that can only be used by one activity at the time.  Each activity $i$ has with it associated \emph{start time} denoted $s_i$ and \emph{finish time} denoted $e_i$ where $s_i \leq e_i$.  The activity takes place during the interval $[s_i, e_i)$.  We say that two activities $i$ and $j$ are \emph{compatible} if the intervals $[s_i, e_i)$ and $[s_j, e_j)$ do not overlap (i.e. $i$ and $j$ are compatible if $s_i \geq e_j$ or $s_j \geq e_i$).  The \emph{activity list problem} is to select a maximum-size set of mutually compatible activities.
\end{problemDescription}

\paragraph{Specification}

We will encode the set of activities by their numeric ID using non-empty list (the empty-set case can be trivially solved and isn't interesting) of type $list^+ Nat$, where
\[
list^+\ Nat ::= wrap\ Nat \ |\ cons\ (Nat, list\ Nat).
\]
The corresponding base functor is $\FF_A(B) = A + (A \times B)$ and $\FF_A(f) = id + (id \times f).$ Given $activitylist : list^+\ Nat \leftarrow list^+\ Nat$ the specification of the problem is $max\, R \cdot \Lambda(activitylist)$ where $R$ is the relation
\[
R = length^\circ \cdot leq \cdot length,
\]
and $length = \cata{zero, succ \cdot outr}$ returns the length of the list.

All that's left now is the definition of $activitylist$.  Since we wish to pick a subset (or in the case of list subsequence), a sensible definition would be
\[
activitylist = \cata{wrap, cons \cdot ok \cup outr}
\]
where the coreflexive $ok$ will check if the newly added activity is compatible with the list of already picked activities:
\[
ok\ (a,x) \equiv (\forall b : b\ inlist\ x : start\ a \geq end\ b \vee start\ b \geq end\ a).
\]

\paragraph{Derivation}

By theorem \ref{th:greedy} a greedy algorithm exists if $activitylist$ is monotonic on $R$.  Using the definition of $activitylist$ we obtain:
\begin{derivation}
  & [wrap, cons \cdot ok \cup outr] \cdot (id + (id \times R)) \subseteq &\\
  & R \cdot [wrap, cons \cdot ok \cup outr] &\\
  \equiv & \dtext{coproduct} &\\
  & [wrap, (cons \cdot ok \cup outr) \cdot (id \times R)] \subseteq &\\
  & [R \cdot wrap, R \cdot (cons \cdot ok \cup outr)] &\\
  \equiv & \dtext{coproduct, composition over join} &\\
  & wrap \subseteq R \cdot wrap \ \wedge &\\
  & (cons \cdot ok \cdot (id \times R) \cup outr \cdot (id \times R)) \subseteq (R \cdot cons \cdot ok \cup R \cdot outr) &
\end{derivation}

The first inclusion is trivially satisfied because $R$ is a preorder, therefore reflexive and so $wrap \subseteq id \cdot wrap \subseteq R \cdot wrap$.  For the second inclusion, we argue:
\begin{derivation}
  & (cons \cdot ok \cdot (id \times R) \cup outr \cdot (id \times R)) \subseteq (R \cdot cons \cdot ok \cup R \cdot outr) &\\
  \equiv & \dtext{universal property of join~\eqref{up:join}} &\\
  & outr \cdot (id \times R) \subseteq (R \cdot cons \cdot ok \cup R \cdot outr) \ \wedge \tageq\label{eq:act:1} &\\
  & cons \cdot ok \cdot (id \times R) \subseteq (R \cdot cons \cdot ok \cup R \cdot outr) \tageq\label{eq:act:2} &
\end{derivation}

By the naturality of $outr$ we get that~\eqref{eq:act:1} holds:
\[
outr \cdot (id \times R) \overset{\eqref{law:naturality-outr}}\subseteq R \cdot outr \subseteq (R \cdot cons \cdot ok \cup R \cdot outr).
\]
However, condition \eqref{eq:act:2} is false.  To see why, let $x$ and $y$ be admissible sequences of activities and $a$ is an activity we wish to add.  Suppose
\[
(a \cons x)\ (cons \cdot ok \cdot (id \times R))\ (a,y),
\]
which implies $xRy$ and $ok\ (a,x)$.  Then at least one of the two must hold:
\begin{align}
(a \cons x)\ (R \cdot outr)\ (a,y) \label{eq:act:3} \\
(a \cons x)\ (R \cdot cons \cdot ok)\ (a,y) \label{eq:act:4}
\end{align}
If $|x| < |y|$ then we can use~\eqref{eq:act:3}, since $(a \cons x)Ry$ and $y\ outr\ (a,y)$.  However, in case when $|x| = |y|$~\eqref{eq:act:3} doesn't hold, and we have to use~\eqref{eq:act:4}.  But that means that $ok\ (a,x)$ should imply $ok\ (a,y)$ and that's not in general true---simply consider $x = [(1,2)], y = [(1,4)], a = (2,3)$.\footnote{The first element of the triple, $id$, is omitted for clarity.}

However, the analysis does suggest a solution: namely if we add an additional requirement that $x \equiv y$ when they are of equal length.  Then $ok\ (a,x)$ trivially does imply $ok\ (a,y)$.  Suppose we refine the ordering $R$ to $R ; S$, where
\[
S = (subseq \cap subseq^\circ) \cup (nil \cdot nil^\circ).
\]
Recalling the definition \ref{def:preorder-composition} from section \ref{sec:impl-preord-comp}, we get that
\[
R ; S = R \cap (R^\circ \Rightarrow S).
\]
In words, $[] (R ; S) []$ and $x (R ; S) y$ if either $|x| < |y|$ or $x \equiv y$.  We will denote $M = (R ; S)$ for brevity.  Since $M \subseteq R$ we can still obtain a greedy algorithm if we prove $activitylist$ is monotonic on $M$ and that the relation $\cata{max\, M \cdot \Lambda (activitylist)}$ can be refined to a function.

To show monotonicity of $activitylist$ on $M$, we have to show that \ref{eq:act:1} and \ref{eq:act:2} hold with $R$ replaced by $M$.  The first equation again holds thanks to the naturality of $outr$.  The second equation holds in the case when $|x| < |y|$ because then $(R ; S) \equiv R$.  Finally, the case when $|x| = |y|$ implies that $x \equiv y$ and then $ok\ (a,x) \Leftrightarrow ok\ (a,y)$.

All that is left to show is the refinement of $\max\, M \cdot \Lambda (activitylist)$ into a function:
\newpage
\begin{derivation}
  & max\, M \cdot \Lambda (activitylist) &\\
  = & \dtext{definition of $activitylist$} &\\
  & max\, M \cdot \Lambda \cata{wrap, cons \cdot ok \cup outr} &\\
  \supseteq & \dtext{monotonicity, greedy theorem \ref{th:greedy}} &\\
  & \cata{max\, M \cdot \Lambda [wrap, cons \cdot ok \cup outr]} &\\
  = & \dtext{power transpose over case~\eqref{law:pt:overcase}} &\\
  & \cata{max\, M \cdot [\Lambda wrap, \Lambda (cons \cdot ok \cup outr)]} &\\
  = & \dtext{power transpose over join~\eqref{law:pt:overjoin}} &\\
  & \cata{max\, M \cdot [\Lambda wrap, cup \cdot \langle \Lambda (cons \cdot ok), \Lambda outr \rangle]} &\\
  = & \dtext{power transpose of functions~\eqref{law:pt:overfun}} &\\
  & \cata{max\, M \cdot [\tau \cdot wrap, cup \cdot \langle \tau \cdot cons \cdot ok, \tau \cdot outr \rangle]} &\\
  = & \dtext{coproduct, split fusion (backward)~\eqref{def:fusion:split}} &\\
  & \cata{max\, M \cdot \tau \cdot wrap, max\, M \cdot cup \cdot (\tau \times \tau) \cdot \langle cons \cdot ok, outr \rangle} &\\
  = & \dtext{introducing binary max: $bmax\, M \ (a,b) \equiv max\, M \ \{a,b\}$} &\\
  & \cata{wrap, bmax\, M \cdot \langle cons \cdot ok, outr \rangle} &
\end{derivation}

The derived algorithm has quadratic time complexity because $ok$ has to check all the activities on the list.  We can improve on this by pre-sorting the input list in descending order by the ending time.  Then it is enough to only check if the activity to be added is compatible with the head of the list and compatibility with the rest follows by transitivity.

Noticing that the list can only grow in size as a result of $cons \cdot ok$ we can simplify this further still by merging $cons \cdot ok$ and $outr$ in single function $ok'$ defined as
\[
ok'\ (a,b) = (start\ a \geq end\ (head\ b) \rightarrow cons\ (a,b), b).
\]
This also eliminates any need for using the relation $M$ directly, since it is implicit in the function $ok'$.  The final result then simplifies to $\cata{wrap, ok'}$.  The catamorphism itself runs in linear time and the resulting algorithm has time complexity $O(n \log n)$ due to sorting.

\paragraph{The program}

The function \textsc{foldr'} is the catamorphism for non-empty lists, and can be found in the appendix.

\programListing[13]{activity}{Activity list program}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% reftex-default-bibliography: ("./bibliography")
%%% End: