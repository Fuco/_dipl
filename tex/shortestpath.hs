import Data.List (delete, nub, nubBy, sortBy)
import Data.Maybe (fromJust)
import Data.Function (on)
import Control.Monad ((>=>))
import Control.Arrow ((&&&))

data PSol = PSol {path::[Char], dist::Float, free::[Char]}
          deriving (Eq, Show)

sp s t = head (filter (((==)t) . head . path) all)
  where init = [PSol [s] 0 (delete s (vertices graph))]
        all = lim (step t) init

step t = sprout t >=> thin

thin :: [PSol] -> Maybe [PSol]
thin = return . nubBy ((==) `on` (head.path)) .
       sortBy (compare `on` dist)

sprout :: Char -> [PSol] -> Maybe [PSol]
sprout t xs = foldr min Nothing xs >>=
              return . uncurry (++) .
              (extend &&& flip delete xs)
  where min x Nothing = if good x then Just x else Nothing
        min x a@(Just y) = if good x && leq x y
                           then Just x else a
        leq = (<=) `on` dist
        good (PSol (x:_) _ f) = x /= t && not (null f)

extend :: PSol -> [PSol]
extend (PSol p@(x:xs) d free) = PSol p d "" :
  foldr (\n ps ->
          if isEdge x n then
            PSol (n:p) (d+edgelen x n) (delete n free):ps
          else ps) [] free

isEdge s t = any (edge s t) graph
edgelen s t = let (_,_,d) = find (edge s t) graph in d
edge s t = \(u,v,_) -> (s == u) && (t == v)

vertices :: [(Char, Char, Float)] -> [Char]
vertices = nub . uncurry (++) . unzip . map proj
  where proj (s,t,_) = (s,t)

{--------------------------}

graph :: [(Char, Char, Float)]
graph = [('a','b',7),('a','c',9),('a','f',14)
        ,('b','a',7),('b','c',10),('b','d',15)
        ,('c','a',9),('c','b',10),('c','d',11), ('c','f',2)
        ,('d','b',15),('d','c',11),('d','e',6)
        ,('e','d',6),('e','f',9)
        ,('f','a',14),('f','c',1),('f','e',9)]

graph2 :: [(Char, Char, Float)]
graph2 = [('a','c',2), ('a','d',6), ('b','a',3)
        ,('b','d',8), ('c','d',7), ('c','e',5)
        ,('d','e',10), ('e','b',1)]

-- from appendix
lim :: ([a] -> Maybe [a]) -> [a] -> [a]
lim step xs = case step xs of
  Just next  -> lim step next
  Nothing    -> xs

pick :: (a -> Bool) -> [a] -> Maybe (a, [a])
pick p xs = pick' p [] xs
  where
    pick' p a (x:xs) = if p x then Just (x, reverse a ++ xs) else pick' p (x:a) xs
    pick' _ a [] = Nothing

left  (v,_,_) = v
mid   (_,v,_) = v
right (_,_,v) = v

find p = head . filter p
