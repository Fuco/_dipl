\newpage
\chapter{Preliminaries}
\label{sec:preliminaries}

Dubbed by some as ``general abstract nonsense''~\cite{MacLane10061997}, both the category theory and algebra of relations require familiarity with relatively large number of definitions and laws in order to enable us do the calculations effectively.  This chapter will therefore serve more as a reference than as a general introduction to the discussed matters.  We will only present definitions related to this thesis and avoid introducing concepts and properties that won't be useful later on.  At each section, we will provide a reference to comprehensive works on the discussed topic.  Since this thesis closely follows ideas and work presented in~\cite{BirddeMoor96:Algebra}, this is the recommended resource that provides systematic development of the theory.

\section{Categories}
\label{sec:categories}

In this section we will introduce some useful concepts from category theory that will be used in the following chapters.  Some familiarity with basic notions of category theory will be assumed.  A comprehensive coverage of the subject for computer scientists can be found in~\cite{DBLP:books/daglib/0068767}.

We will be working in category $\REL$.  The objects of this category are sets and the arrows are relations between sets.  $\REL$ is also an allegory, a category enhanced with intersection $\cap$, converse $^\circ$ and comparison operator $\subseteq$.  Allegories aim to generalize the notion of categories in the same way relations generalize functions.  However, in the category of relations these extra operators have their usual meaning from the algebra of relations and set theory and we will therefore not explore the rich world of allegories much further.  For more details on allegories we refer to~\cite{FreydPJ:cata}.

\subsection{Functors}
\label{sec:functors}
\index{functor|(}

A functor is a type of mapping between categories.  It can be thought of as homomorphism between categories, that is a structure-preserving map.

\begin{definition}
  Let $\CC$ and $\DC$ be categories.  A \emph{functor} $\FF : \CC \leftarrow \DC$ is a mapping that satisfies following properties:
\begin{itemize}
\item to each object $X \in \DC$ assigns an object $\FF X \in \CC$
\item to each arrow $f : X \leftarrow Y \in \DC$ assigns an arrow $\FF f : \FF X \leftarrow \FF Y \in \CC$
\item $\FF(id_X) = id_{\FF X}$ for every object $X \in \DC$
\item $\FF (f \cdot g) = \FF f \cdot \FF g$ for all arrows $f : X \leftarrow Y, \, g : Y \leftarrow Z \in \DC$
\end{itemize}
\end{definition}

Functors can be generalized to \emph{bifunctors}, that is functors that take two arguments, or \emph{multifunctors} taking any number of arguments.  We will encounter bifunctors when defining data types.

Functors can also be composed in the natural way.  Let $\FF : \AC \leftarrow \BC$ and $\GF : \BC \leftarrow \CC$, then the composition is $(\FF \cdot \GF) : \AC \leftarrow \CC,\, (\FF \cdot \GF) f = \FF (\GF f)$.\index{composition!of functors}  Every category $\CC$ has an \emph{identity}\index{functor!identity} functor $id_\CC : \CC \leftarrow \CC$.  In the following we will often omit the composition operator and assume the functor composition associates to the right, therefore $\FF \GF C = \FF (\GF C)$.

One example of a functor is the identity functor, sending each object and arrow to itself.  Other useful example is the \emph{constant}\index{functor!constant} functor $\KF_C : \CC \leftarrow \DC$ which maps all objects of $\DC$ to object $C \in \CC$ and all arrows to $id_C$.  The \emph{powerset}\index{functor!powerset} functor $\PF : \FUN \leftarrow \FUN$ maps set $C$ to the powerset $\PF C$ which is defined as
\begin{align}
\PF C = \{x \st x \subseteq C \} \label{def:powerfunctor}
\end{align}
and a function $f$ to function $\PF f$ that applies $f$ to all elements of the set.  Last example is the \emph{existential image}\index{functor!existential image} functor $\EF : \FUN \leftarrow \REL$ which is closely related to the power functor.  It maps set $C$ to the powerset $\PF C$ and relation $R$ to the existential image operator defined
\begin{align}
(\EF R) X = \{ a \st x \in X \wedge aRx \}. \label{def:ex-image}
\end{align}
We will discuss these more in section~\ref{sec:relations} on relations.

Later in this section we will use functors to define and describe data types.  To build more complex types from simpler structures, a useful concept are the \emph{polynomial} functors.

\begin{definition}\label{def:polyfunctors}\index{functor!polynomial}
  Functors built up from constant functors, products and coproducts are called \emph{polynomial}.  They are defined inductively:
  \begin{itemize}
  \item The identity functor $id$ and constant functor $\KF_C$ are polynomial.
  \item If $\FF$ and $\GF$ are polynomial, so is their composition $\FF \GF$ and their pointwise sum and product defined as:
\begin{align*}
  (\FF \times \GF)\ f =&\  \FF f \times \GF f \\
  (\FF + \GF)\ f =&\  \FF f + \GF f
\end{align*}
  \end{itemize}
\end{definition}

\begin{example}
  The functor $\FF$ defined on objects as $\FF X = C + C \times X$ and on arrows as $\FF f = id_C + id_C \times f$ is a polynomial functor.  It can be constructed as $\FF = \KF_C + (\KF_C \times id).$
\end{example}
\index{functor|)}

\subsection{Natural transformations}
\label{sec:natur-transf}
\index{natural transformation}

Natural transformations provide a method of transforming functors into different functors while respecting the internal structure of the categories involved, that means the composition of morphisms in these categories.  A natural transformation can be viewed as ``morphism of functors''.

\begin{definition}
  Let $\CC, \DC$ be two categories and $\FF, \GF : \CC \leftarrow \DC$ two functors between them.  A \emph{natural transformation} $\phi$ to $\FF$ from $\GF$ is a collection of arrows $\phi_D : \FF D \leftarrow \GF D$, one for each object $D \in \DC$ such that
\[
\FF f \cdot \phi_D = \phi_C \cdot \GF f
\]
holds for every arrow $f : C \leftarrow D \in \DC$.  That is, the following diagram commutes:

\begin{diagram}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
  {
     \FF D & \GF D \\
     \FF C & \GF C \\};
  \path[-stealth]
    (m-1-1) edge node [left] {$\FF f$} (m-2-1)
    (m-1-2) edge node [above] {$\phi_D$} (m-1-1)
                edge node [right] {$\GF f$} (m-2-2)
    (m-2-2) edge node [below] {$\phi_C$} (m-2-1);
\end{diagram}
\end{definition}

To indicate that a transformation is natural we write shortly $\phi : \FF \leftarrow \GF$.

Natural transformations can be composed together component-wise.  That is, if $\phi : \FF \leftarrow \GF$ and $\psi : \GF \leftarrow \HF$ are natural transformations, then $\phi \cdot \psi : \FF \leftarrow \HF$ is natural and defined as\index{composition!of natural transformations}
\[
(\phi \cdot \psi)_D = \phi_D \cdot \psi_D.
\]

\begin{example}
As an example of a natural transformation consider the function $union_C : \PF C \leftarrow \PF \PF C$.   The naturality condition is
\[
\PF f \cdot union = union \cdot \PF \PF f.
\]
This condition states that it doesn't matter if we first unite a system of sets and then map a function over it, or first map a function over each individual set and then unite the results.  Spotting natural transformations will be very useful in derivations where they can simplify the equations substantially.
\end{example}

\subsection{Initial algebras and catamorphisms}
\label{sec:init-algebr-catam}
\index{initial algebra|(}

To model data types we will use the concept of \emph{Initial algebras}.  We will show how the familiar \textsc{foldr} function from functional programming can be extended to arbitrary data types expressed as initial algebras.  The notation will follow that of~\cite{vene2000categorical}, which is also recommended for further inquiry on various recursive patterns useful in functional programming.

\begin{definition}\index{falgebra@$\FF$-algebra}
Let $\FF : \CC \leftarrow \CC$ be an endofunctor on category $\CC$.  An $\FF$-\emph{algebra} is a pair $(C, \varphi)$ where $C$ is an object of $\CC$ and $\varphi : C \leftarrow \FF C$ is an arrow in $\CC$.  The object $C$ is called the \emph{carrier} of the algebra and the functor $\FF$ is called \emph{signature} of the algebra.
\end{definition}

The carrier of the algebra will often be omitted and we will refer to the algebra only by its mapping part.

\begin{example}
  An algebra $(Nat, +)$ is the algebra of natural numbers with addition, where the signature functor is $\FF X = X \times X,\, \FF f = f \times f.$ Indeed, the function $+$ has the correct type $+ : Nat \leftarrow Nat \times Nat$ as dictated by the functor $\FF$.
\end{example}

\begin{example}
  An algebra $(\mathbb{R^\times}, one, \times, \lambda x . 1/x)$ represents the group of real numbers without zero with multiplication as operation.  The functor describing this algebra is $\FF X = 1 + (X \times X) + X$.  It is composed of three operations: first returning the neutral element, second is the group operation, and third assigns to each element its inverse.
\end{example}

\begin{definition}\label{def:homomorfism}
Let $(C, \varphi)$ and $(D, \psi)$ be two $\FF$-algebras.  A \emph{homomorphism} to $\varphi$ from $\psi$ is an arrow $f : C \leftarrow D$ in the category $\CC$ such that
\[
f \cdot \psi = \varphi \cdot \FF f,
\]
that is, it makes the following diagram commute:

\begin{diagram}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
  {
     D & \FF D \\
     C & \FF C \\};
  \path[-stealth]
    (m-1-1) edge node [left] {$f$} (m-2-1)
    (m-1-2) edge node [above] {$\psi$} (m-1-1)
                edge node [right] {$\FF f$} (m-2-2)
    (m-2-2) edge node [below] {$\varphi$} (m-2-1);
\end{diagram}
\end{definition}

For any $\FF$-algebra, the identity arrow on its carrier is an endomorphism (a homomorphism from it to itself).  The composition of two homomorphisms is again a homomorphism.  This means that we can define a category $\ALG{\FF}$ where the objects are $\FF$-algebras and the arrows are homomorphisms between them.  Initial algebras will be precisely the initial objects of these categories.  The initial object need not exist for every possible functor, however they always do exist for polynomial functors~\cite{DBLP:books/daglib/0068171} we will use throughout this thesis.

\begin{definition}\index{catamorphism}
An $\FF$-algebra $(T, \alpha)$ is the \emph{initial} $\FF$-\emph{algebra} if for any $\FF$-algebra $(C, \varphi)$ there exists a unique homomorphism $\cata{\varphi} : C \leftarrow T$ making the following diagram commute:
\begin{diagram}
  \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
  {
     T & \FF T \\
     C & \FF C \\};
  \path[-stealth]
    (m-1-1) edge node [left] {$\cata{\varphi}$} (m-2-1)
    (m-1-2) edge node [above] {$\alpha$} (m-1-1)
                edge node [right] {$\FF \cata{\varphi}$} (m-2-2)
    (m-2-2) edge node [below] {$\varphi$} (m-2-1);
\end{diagram}
This homomorphism is characterized by the following universal property:\index{universal property!of catamorphism}
\begin{align}
h &= \cata{\varphi} ~~ \equiv ~~ h \cdot \alpha = \varphi \cdot \FF h. \label{up:cata}
\end{align}
The arrows of the form $\cata{\varphi}$ are called \emph{catamorphisms} and are generalizations of the traditional \textsc{foldr} function on lists.
\end{definition}

\begin{lemma}
\index{fusion law!for catamorphisms}
\index{reflection law!for catamorphisms}
\label{law:fusion-cata}
  From the definition of catamorphism we get the following laws:
  \begin{enumerate}
  \item reflection law: $\cata{\alpha} = id$
  \item fusion law: $h \cdot \varphi = \psi \cdot \FF h \Rightarrow h \cdot \cata{\varphi} = \cata{\psi}$
  \end{enumerate}
\end{lemma}

\begin{proof}
The first part is trivial.  When $\alpha$ is the initial algebra, it means there is a unique homomorphism to any other algebra.  Since the identity (homo)morphism exists for any algebra, this must be the unique homomorphism $\cata{\alpha}$.

The second part can be conveniently proved using a diagram:
\begin{diagram}
   \matrix (m) [matrix of math nodes,row sep=3em,column sep=4em,minimum width=2em]
  {
     \FF C & \FF D & \FF T \\
          C &      D &      T \\};
  \path[-stealth]
    (m-1-1) edge node [left] {$\psi$} (m-2-1)
    (m-1-2) edge node [above] {$\FF h$} (m-1-1)
                edge node [right] {$\varphi$} (m-2-2)
    (m-2-2) edge node [above] {$h$} (m-2-1)
    (m-1-3) edge node [above] {$\FF \cata{\varphi}$} (m-1-2)
                edge node [right] {$\alpha$} (m-2-3)
    (m-2-3) edge node [above] {$\cata{\varphi}$} (m-2-2)
    (m-2-3) edge [dashed,bend left] node [below] {$\cata{\psi}$} (m-2-1);
\end{diagram}
The diagram commutes because the left square commutes (by assumption) and the right square commutes by the definition of catamorphism.  Because there exists a unique homomorphism from $\alpha$ to $\psi$ it must be the composition $h \cdot \cata{\varphi}.$

\qed
\end{proof}

It is useful to think of catamorphisms as replacing the constructors of the initial algebra with operations from another algebra with the same signature.  The reflection law states that if we replace the constructors with themselves, nothing changes.

To illustrate the abstract ideas presented above, let us now give two examples of inductive data types defined in the language of initial algebras.  The notation used for \emph{case} is the same as with relations in section~\ref{sec:products-coproducts}, specifically definition~\ref{def:case}.  The examples are roughly based on those found in~\cite{vene2000categorical}, however, they are the standard examples of inductive data types found in functional programming.

\paragraph{Natural numbers}
\index{Natural numbers}
In functional programming, the type of natural numbers is commonly defined algebraically as:
\[
Nat ::= zero \st succ\ Nat.
\]
This declaration gives us two constructors, namely $zero : Nat \leftarrow 1$ and $succ : Nat \leftarrow Nat$.  Using \emph{case}, these constructors can be combined into a single function, forming an algebra $[zero, succ] : Nat \leftarrow \FF Nat$ of some functor $\FF$.  The functor $\FF$ describes the structure of the type and is defined as $\FF X = 1 + X$, $\FF h = id_1 + h$ where 1 is the terminal object of $\FUN$.  The functor $\FF$ is polynomial, so $\ALG{F}$ has an initial object and we can take the algebra $[zero, succ]$ as the initial $\FF$-algebra.  It should be noted that $zero$ and $succ$ are arbitrary labels we chose for the constructors of the initial algebra.  Their names suggest a convenient interpretation, but do not restrict us in any way.

Let's now look at the catamorphisms from this initial algebra.  Suppose we have different $\FF$-algebra $[c, f]$ where $c : C \leftarrow 1$ and $f : C \leftarrow C$.  By definition \ref{def:homomorfism}, a homomorphism $h : C \leftarrow Nat$ from $[zero, succ]$ to $[c, f]$ must satisfy the following two equations:
\begin{alignat*}{3}
  h&\, zero & &= c \\
  h&\, (succ\ x) & &= f\, (h\, x).
\end{alignat*}
Because there exists only one homomorphism from the initial algebra, the catamorphism must be the unique solution of the above system.

Our intuition about catamorphisms replacing the constructors is now made clear.  For example, if we apply $h$ to the representation of the number 3 written as $succ\, (succ\, (succ\ zero))$, we get:
\begin{align*}
h\, (succ\, (succ\, (succ\ zero))) & = f\, (h\, (succ\, (succ\ zero))) = \ldots \\
& = f\, (f\, (f\, (h\, zero))) = f\, (f\, (f\, c)).
\end{align*}
The catamorphism for any algebra $[c, f]$ is then the function $n \mapsto f^n(c)$.

\paragraph{Lists}
\index{Lists}
We will once again start by the traditional definition found in functional programming.  A list parametrized by type $A$ can be defined as:
\[
list\, A ::= nil \st cons\ (A, list\, A).
\]
This time we have two constructors parametrized by type $A$.  The first is $nil : list\, A \leftarrow 1$, the second $cons : list\, A \leftarrow (A, list\, A)$.  The corresponding functor will now have to take the type parameter into account.  This is achieved by extending it to bifunctor $\FF(A, X) = 1 + A \times X$.  We can fix the first parameter $A$ to obtain a regular functor $\FF_A X$ when it will be convenient.

Given two functions $c : C \leftarrow 1$ and $f : C \leftarrow A \times C$ we can use the case operator to form an $\FF$-algebra $[c, f]$.  The homomorphism $h : C \leftarrow list\, A$ has to satisfy the following equations:
\begin{alignat*}{2}
  h& \cdot nil & &= c  \\
  h& \cdot cons & &= f \cdot (id \times h).
\end{alignat*}
If written out using points we will obtain the usual implementation of \textsc{foldr} from functional programming.
\index{initial algebra|)}

\subsection{Terminal algebras, anamorphisms and hylomorphisms}
\index{terminal algebra}
The categorical dual to the initial object and initial algebra is the \emph{terminal object} and \emph{terminal algebra}.  The unique homomorphism \emph{from} every $\FF$-algebra \emph{into} the terminal algebra is called \emph{anamorphism}\index{anamorphism}.  The initial and terminal objects can be, if they exist at all, generally distinct.  Our category of interest, the category of relations $\REL$, is self-dual, therefore the terminal objects are also initial and vice versa, with same holding for the respective algebras.  Every anamorphism can then be expressed as a converse of catamorphism.

\begin{definition}\index{hylomorphism}
  Let $\varphi$, $\psi$ be $\FF$-algebras.  The composition $\cata{\psi} \cdot \cata{\varphi}^\circ$ of an anamorphism $\cata{\varphi}^\circ$ followed by a catamorphism $\cata{\psi}$ is called \emph{hylomorphism}.
\end{definition}

Hylomorphisms model recursive programs, where the anamorphism builds the call tree, and catamorphism ``folds'' it to the final result.  We will use hylomorphisms in chapter \ref{sec:optim-probl} to model a subclass of optimization problems.

Using the Knaster-Tarski fixpoint theorem (\cite{Knaster:54.0091.04}, \cite{Tarski:0064.26004}) one can show that hylomorphisms can also be represented as least fixpoints of certain recursive equations.  If $\phi$ is a monotonic mapping, we will use notation $(\mu X : \phi X)$ to mean the least solution to the equation $X = \phi X$.

\begin{theorem}\index{Hylomorphism theorem}
  Suppose that $R : A \leftarrow \FF A$ and $S : B \leftarrow \FF B$ are two $\FF$-algebras.  Then $\cata{R} \cdot \cata{S}^\circ : A \leftarrow B$ is given by
\[
\cata{R} \cdot \cata{S}^\circ = (\mu X : R \cdot \FF X \cdot S^\circ).
\]
\end{theorem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% reftex-default-bibliography: ("./bibliography")
%%% End:
