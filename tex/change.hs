import Data.List (minimumBy)
import Data.Function (on)

mc d = snd . head . mc' (encode d) [(0,[])]
mc' _ a 0 = a
mc' d a n = mc' d (new : a) (n - 1)
  where new = minimumBy (compare `on` fst)
              (zipWith sum' d a)
        sum' (n,d) (m, ds) = (n+m, d:ds)

encode d@(x:_) = nn >>= (zip (1:[1/0..]) .
                         uncurry enumFromTo)
  where nd = reverse (x+1:d)
        nn = zip nd (map (subtract 1) (tail nd))
