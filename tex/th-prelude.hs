-- Implements the limit operator.  The arguments are the
-- step function and initial value.
lim :: ([a] -> Maybe [a]) -> [a] -> [a]
lim step xs = case step xs of
  Just next  -> lim step next
  Nothing    -> xs

-- Pick the first item satisfying `p' and return `Just'
-- this item and the rest of the list without it.  If no
-- item is found, return `Nothing'
pick :: (a -> Bool) -> [a] -> Maybe (a, [a])
pick p xs = pick' p [] xs
  where
    pick' p a (x:xs) = if p x then
                         Just (x, reverse a ++ xs)
                       else pick' p (x:a) xs
    pick' _ a [] = Nothing

find :: (a -> Bool) -> [a] -> a
find p = head . filter p

-- catamorphism for non-empty lists
foldr' :: (a -> b) -> (a -> b -> b) -> [a] -> b
foldr' f g [a] = f a
foldr' f g (a:x) = g a (foldr' f g x)
