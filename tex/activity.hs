import Data.Function (on)
import Data.List (sortBy)

type Activity = (Int, Int, Int)

activitylist a = foldr' (:[]) ok' (sort' a)
  where sort' = sortBy (flip compare `on` end)

ok' a l@(b:_) = if start a >= end b then a:l else l

start (_,s,_) = s
end (_,_,e) = e

{-----------------}

-- activitylist = cata1list wrap (bmax r . cons') . sortBy (flip compare `on` end)

cons' = pair (cons . ok, outr)
ok :: (Activity, [Activity]) -> (Activity, [Activity])
ok (a, l@(b:_)) = if geq (start a, end b) then (a,l) else (a,[])
r x y = leq (length x, length y)


act = [(1,1,4),(2,3,5),(3,0,6),(4,5,7)
      ,(5,3,8),(6,5,9),(7,6,10),(8,8,11)
      ,(9,8,12),(10,2,13),(11,12,14)] :: [(Int, Int, Int)]

{--- prelude ---}
outl (a,b) = a
outr (a,b) = b
pair (f,g) a = (f a, g a)

cons (a,b) = a:b
wrap a = [a]

cata1list f g [a] = f a
cata1list f g (a:x) = g (a, (cata1list f g x))

leq :: Ord a => (a,a) -> Bool
leq = uncurry (<=)

geq :: Ord a => (a,a) -> Bool
geq = uncurry (>=)

bmin p (x,y) = if p x y then x else y
bmax p (x,y) = if p x y then y else x
