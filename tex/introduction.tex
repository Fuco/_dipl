\newpage
\chapter{Introduction}

The goal of this thesis is to study methods of deriving algorithms from specifications using equational reasoning.  In other words, we will calculate programs.  The traditional approach to \emph{Algorithm Design} is ``to think hard about the problem, then write down the solution''\footnote{Also known as the \emph{Feynman Algorithm}, due to Murray Gell-Mann.}.  However, there are several useful strategies that can help in this certainly difficult task.  The classical examples present in each computer science curriculum are dynamic programming, greedy algorithms, divide and conquer and exhaustive search.  Their use is nowadays a standard part of every endeavor pertaining to efficient algorithm design.

On the other hand is the lesser known and used \emph{algebraic} approach.  Here, a rigorous mathematical reasoning is applied to solve a particular problem.  Using some mathematical framework, certain patterns of algorithmic design are expressed as \emph{theorems}.  Each of these theorems will come with certain preconditions, which if satisfied, then tell us that the selected strategy works and leads to particular form of solution or more efficient implementation.

Even though this is rarely used as a straight-up method of algorithm design, many of the rigorous theorems are used in compilers as parts of optimization engines.  This in particular is extremely fitting for pure functional languages, such as Haskell\footnote{The Haskell Programming Language, \url{http://www.haskell.org/}}, where the programs themselves are specified as equations and theorems, written down using a certain formalism.  In some cases, these techniques produce even more efficient programs~\cite{mainland13haskellbeatsc} than the traditional ``fast'' languages for mathematical computations, such as The C Language.  An ever-expanding list of related papers maintained by Haskell community can be found on the Haskell wiki\footnote{\url{http://www.haskell.org/haskellwiki/Research_papers/Compilation}}.

To use this algebraic approach effectively, we will first need to choose a mathematical framework in which to operate.  Our choice in this thesis is the categorical algebra of relations.  This approach enhances the classical algebra of relations with useful concepts from category theory.  We will use the categorical concepts to work with data types and formulate theorems about them.  The relational approach, as opposed to functional, gives us more freedom in specifications and proofs that would be cumbersome using functional approach.  Indeed, notions such as non-determinism or partial functions are readily available to us in form of relations.

Concrete algorithms are obtained by first specifying the problem mathematically, then checking the conditions of related theorems.  The solution will usually take form of a relation, characterized either directly by a formula, or as a solution to a system of one ore more recursive equations.  This relation is then \emph{refined} to a function and rewritten into a functional programming language of choice---in our case it will be Haskell.

This thesis will follow up the work presented in~\cite{BirddeMoor96:Algebra}.  We will briefly present the achieved theoretical results related to discussed design pattern, and then apply them to solve particular problems in a similar format as in the referenced book.  As far as we know, the selected problems were not previously solved in this manner.

\paragraph{Outline}

The thesis consists of two main parts.  The first part of the thesis contains mostly theoretical background necessary to fully appreciate and understand the following derivations.  It is mostly of interest to those readers who had less exposure to category theory and/or algebra of relations.  An experienced reader can safely skip these chapters, as all the lemmas proved there are properly back-referenced, and also to be found in the index.  The second part then contains the case studies of the problems themselves.

In chapter~\ref{sec:notation} we introduce some basic notational conventions used throughout the thesis.  In chapter~\ref{sec:preliminaries} we introduce some useful notions from category theory and the calculus of relations.  This chapter forms a stand-alone compilation of the various definitions and theorems, and can be also used as a quick reference for those who seek information on basics of program calculation and related topics.  In each section, we provide further sources for in-depth study.

In chapter~\ref{sec:optim-probl} we introduce the class of \emph{Optimization problems} and explore two algorithm design principles: \emph{Greedy Algorithms} and \emph{Dynamic Programming}.  We then present a derivation of a solution to selected problem for each of these principles.  Before each case study, a brief theoretical introduction is included, with highlights of the relevant theorems used in the derivation.

Finally, in chapter~\ref{sec:limits} we present the \emph{Limit operator} as an alternative approach to problem specification.  This operator was introduced by~\cite{Curtis96arelational} in response to some difficulties found in the earlier approach of~\cite{BirddeMoor96:Algebra}.  We will prove theorem~\ref{th:dynth} and demonstrate its use on the \emph{single-pair shortest paths problem} from Graph theory.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: