import Data.List
import Data.Function (on)
import Control.Arrow
import Control.Monad

mc d = snd . head . mc' (encode d) [(0,[])]
  where mc' _ a 0 = a
        mc' d a n = mc' d (new : a) (n - 1)
          where new = minimumBy (compare `on` fst) (zipWith sum' d a)
                sum' (n,d) (m, ds) = (n+m, d:ds)

encode d@(x:_) = nn >>= (zip (1:[1/0..]) . uncurry enumFromTo)
  where nd = reverse (x+1:d)
        nn = zip nd ((map (subtract 1)) (tail nd))

-- encode = concat .
--          map (zip (1:[1/0..]) . uncurry enumFromTo) .
--          ap zip (map (subtract 1) . tail) .
--          reverse . ((:) =<< ((+1) . head))

-- add memoization!
-- mc :: [Int] -> Int -> Int
-- mc _ 0 = 0
-- mc den n = 1 + mini (map (mc den) b)
--   where b = filter (>= 0) (map ($ n) (fmap (flip (-)) den))

mmc :: [Int] -> [Int] -> Int -> Int -> [Int]
mmc den mem m n | m <= n = mmc den new (m+1) n
                | otherwise = mem
  where new = (1 + mini (map (mem !!) b)):mem
        b = filter (< m) den

mmc' :: [Int] -> Int -> [Int]
mmc' den n = reverse $ mmc (map (+ (-1)) den) [0] 1 n

k = 1/0
input = [1,1,k,k,1]
f x = let y = minimum . zipWith (+) input $ x in y : x
--foldr (\n a -> (minimum (zipWith (+) n a)) : a) [0] (replicate 40 input)
input' = [(1,1),(1,2),(k,3),(k,4),(1,5)]
f' x = let y = minimumBy (compare `on` fst) . zipWith (uncurry (***) . ((+) *** (:))) input' $ x in y : x

-- original
mctable d = snd . head . foldr
            ((:) <=<
             ((minimumBy (compare `on` fst).) .
              zipWith (uncurry (***) . ((+) *** (:))))) [(0,[])] . flip replicate d

-- mctable ktora skracuje zoznam = setri pamat
mct d a n = if n == 0
            then a
            else let new = ((minimumBy (compare `on` fst).) . zipWith (uncurry (***) . ((+) *** (:)))) d a
                 in mct d (new : (take (length d) a)) (n - 1)

mctable' d a n = if n == 0
                 then a
                 else mctable' input' (((:) <=<
                                        ((minimumBy (compare `on` fst).) .
                                         zipWith (uncurry (***) . ((+) *** (:))))) d a) (n - 1)

codeInput = concat . map (zip (1:[1/0..]) . uncurry enumFromTo) .
            ap zip (map (subtract 1) . tail) . reverse . ((:) =<< ((+1) . head))

codeInput' d@(x:xn) = zip x' (map (subtract 1) . tail $ x')
  where x' = reverse ((x + 1) : d)


-- \n a -> f n a : a  <=>  \n a -> (:) (f n a) a  <=>  (:) <=< f

thinspl :: [Int] -> Int -> [(Int,Int)]
thinspl d = if' ((==0).snd.head) (wrap.head) id .
            dropWhile ((<0).snd) .
            map (fst &&& uncurry subtract) .
            zip d . repeat

-- mc' :: [Int] -> Int -> [Int]
-- mc' d n = if n == 0
--           then []
--           else minimumBy (compare `on` length) .
--                map (uncurry (:) . (id *** mc' d)) .
--                thinspl d $ n

if' p t e x = if p x then t x else e x

wrap x = [x]

anagen x = unfoldr f
  where f n = if n == 0 then Nothing else Just (x, n-1)
catagen = foldr f [0]
  where f n a = minimum (zipWith (+) n a) : a

hylogen x n = if n == 0 then [0] else f x (hylogen x (n-1))
  where f n a = minimum (zipWith (+) n a) : a

mini [] = 0
mini x = minimum x

gen :: Int -> [Int] -> [Int]
gen n ds = unfoldr ex n
  where
    ex x = case filter ((>=0) . snd) . map (\y -> (y, x - y)) $ ds of
      []     -> Nothing
      (y:ys) -> Just y
--[1,2,3,4]

partition :: [a] -> [[[a]]]
partition = foldr (\a xs -> [[a]:y | y <- xs] ++ [(a:x):y | (x:y) <- xs]) [[]]


partition' :: [Int] -> Int -> [[Int]]
partition' d = map (map sum) .
               foldr (\a xs -> [[a]:y | y <- xs, if not (null y) then elem (length (head y)) d else True] ++
                               [(a:x):y | (x:y) <- xs]) [[]] . flip replicate 1

getall :: [Int] -> Int -> [[Int]]
getall den n | n <= 0 = [[]]
             | otherwise = [ d:x | d <- den, x <- getall den (n - d), (n - d) >= 0]

-- vzorka = [1,2,3,1,2,4]
-- pt = map (len' . (uncurry dropWhile) . ((not.) . flip isPrefixOf . head &&& tail) . tails) . inits
--   where len' x = if null x then -1 else length . head $ x
