\newpage
\chapter{Limits}
\label{sec:limits}

The previous chapter discussed optimization problems and their solutions using greedy and dynamic programming strategies.  The generators of feasible solutions were expressed using anamorphisms or catamorphisms.  However, some generators can not be expressed using these morphisms or would require great deal of effort to do so.

An approach to solve these problems is presented in~\cite{Curtis96arelational} by introducing the \emph{Limit operator}.  The Limit operator models a simple loop in the language of relations.  It generalizes the methods of chapter~\ref{sec:optim-probl} as anamorphisms and catamorphisms can be both modeled using the Limit operator.  Also, a loop often provides simpler or more natural way to implement the generator.

In this chapter we will introduce the basics of the Limit operator and prove theorem~\ref{th:dynth} about its use in the context of Dynamic programming.  We will then demonstrate this theorem on the problem from Graph theory concerning finding the shortest path between a pair of vertices.

\section{Introducing the limit operator}

In this section we will provide formal definition of the limit operator and some useful properties that will be used in the following calculations.

\begin{definition}\label{def:limit}\index{limit}
Let $T : A \leftarrow A$ be a binary relation.  The \emph{limit} of $T$ is defined as:
\[
lim\ T = notdom\ T \cdot T^*.
\]
We say that the relation $T$ is the \emph{constructing relation} or the \emph{body}.
\end{definition}
\vspace{\parskip}

At each stage, $T$ is performed on the partial solution, extending it one step towards the complete solution.  The construction ends when $T$ can not be applied any more as signified by relation $notdom\ T$.

The relation $lim\ T$ can also be described as the least solution of the recursive equation
\[
X = notdom\ T \cup X \cdot T.
\]
The Knaster-Tarski fixpoint theorem gives us that
\begin{align}
notdom\ T \cup X \cdot T \subseteq X \Rightarrow lim\ T \subseteq X. \label{law:limits:recursion}
\end{align}
The following properties of $lim$ are a direct consequence of the definition:
\begin{align}
lim\ T \cdot notdom\ T = notdom\ T \\
notdom\ T \cdot (lim\ T)^\circ = notdom\ T \label{eq:notdom-lim}
\end{align}

The limit operator gives us yet another way to specify optimization problems, besides those mentioned in chapter~\ref{sec:optim-probl}.  The most basic specification has the form
\[
min\, R \cdot \Lambda (lim\ T),
\]
that is: build all possible solutions using the constructor $T$ and pick the best with respect to preorder $R$.

We will illustrate the generation process on the following example.

\begin{example}[The 0-\!1 Knapsack Problem]
In the \emph{0-\!1 Knapsack Problem} we are given a set of $n$ items $\{x_1, x_2, \ldots, x_n \}$, where $x_i$ has value $v_i$ and weight $w_i$.  The maximum weight the bag can carry is $W$.  Then the problem is to maximize $\sum_{i=1}^n v_ix_i$ subject to $\sum_{i=1}^n w_ix_i \leq W$ where $x_i \in \{0, 1\}$.

A packing can be represented as a triple (weight, value, items).  The constructing relation $T$ will at each step decide to either add an item to the packing or throw it away.  It may be defined in the following way:
\begin{alignat*}{3}
  (w, v, B, I \setminus \{i\}) ~& T &~ (w, v, B, I) &~~ \text{if } i \in I \\
  (w + w_i, v + v_i, B \cup \{i\}, I \setminus \{i\}) ~& T &~ (w, v, B, I) &~~ \text{if } w + w_i \leq W, i \in I
\end{alignat*}
where $B$ is the set of chosen items and $I$ is the set of items yet to be processed.  The input to the process is $ (0, 0, \emptyset, Items)$ where $Items$ is the set of all the items to consider.
\end{example}

This example illustrates that generating solutions using limits is not ``sequential'', in the sense that catamorphisms consume the data structure in sequence, and anamorphisms generate the structure as a stream.  With limits, we can consume or generate the data without any apparent fixed order or structure.  This adds certain degree of flexibility and allows us to specify problems in a ``bottom-up'' way, starting from the simplest blocks iteratively building the solution.  Many algorithms from Graph theory use this approach, for example Prim's and Jarnik's algorithms for minimal spanning tree.  Another example is Huffman encoding.

\section{Limits and dynamic programming}

The partial solutions are generated in the following manner.  The process starts with a singleton set---the initial input.  At each step, $T$ is applied in all possible ways to the current set of partial solutions.  Once no application of $T$ is possible, the generating process ends.  This process can be visualized as building a rooted directed acyclic graph (RDAG), with root being the initial input, and each level as one successive application of $\Lambda T$.  The leaves are the completed solutions that are no longer in the domain of $T$.

The recurring theme in dynamic programming is elimination of unnecessary computation.  A naive implementation of the specification $min\, R \cdot \Lambda (lim\ T)$ would be to generate all the solutions and then select the best one with respect to $R$.  We will now present one possible scheme of reduction of the set of partial solutions which eliminates the computation of future solutions that will provably produce sub-optimal results.  The main idea to prune the RDAG that is generated by successive applications of $\Lambda T$ and is based on thinning introduced in section~\ref{sec:thinning}.

\subsection{Sprouting and thinning}

To capture the idea of iteratively extending the set of partial solutions we will introduce the concept of \emph{sprouting.}

\begin{definition}\index{sprouting}
Let $luni\ (x,y) = x \cup y, \text{if } x \not = \emptyset$ and $cup\ (x,y) = x \cup y.$ Then
\[
sprouts\ T = cup \cdot (\EF T \cdot \PF dom\ T \times id) \cdot luni^\circ.
\]
\end{definition}
\vspace{\parskip}

In words, $sprouts\ T$ takes the input set and partitions it into two parts.  On the first part $T$ is applied in all possible ways while the second part is retained without change.  These two sets are then added together and form the input for the next step, or the set of completed solutions if it is no longer possible to apply $T$ to any partial solution in the set.

It is also possible to define more specified variants of the sprouting relation.  One particularly useful is a version that expands only one partial solution at each step.  It might be defined as
\begin{align}
sprout\ T = cup \cdot (\Lambda T \cdot dom\ T \times id) \cdot lcons^\circ,
\end{align}
where $lcons\ (x,y) = \{x\} \cup y, x \not = \emptyset$.  A simple calculation shows that
\[
sprout\ T \subseteq sprouts\ T.
\]

The following lemma will be useful in the proof of theorem~\ref{th:dynth}.

\begin{lemma}\label{lem:sprout1}
Let $R, S$ be two binary relations.  Then:
\[
\in \cdot \ cup \cdot (\EF R \times \EF S) \cdot cup^\circ \subseteq (R \cup S) \ \cdot \in.
\]
\end{lemma}

\begin{proof}
\begin{derivation}
  & \in \cdot \ cup \cdot (\EF R \times \EF S) \cdot cup^\circ &\\
= & \dtext{definition of $\EF$} &\\
& \in \cdot \ cup \cdot (\Lambda (R \ \cdot \in) \times \Lambda (S \ \cdot \in)) \cdot cup^\circ &\\
= & \dtext{definition of cross~\eqref{def:cross}, outl and outr functions} &\\
& \in \cdot \ cup \cdot \langle \Lambda (R \ \cdot \in \cdot \ outl), \Lambda (S \ \cdot \in \cdot \ outr) \rangle \cdot cup^\circ &\\
= & \dtext{power transpose over join (reverse)} &\\
& \in \cdot \ \Lambda (R \ \cdot \in \cdot \ outl \cup S \ \cdot \in \cdot \ outr ) \cdot cup^\circ &\\
= & \dtext{cancellation law for power transpose~\eqref{law:pt:cancel}} &\\
& (R \ \cdot \in \cdot \ outl \cup S \ \cdot \in \cdot \ outr ) \cdot cup^\circ &\\
\subseteq & \dtext{property of join} &\\
& (R \ \cdot \in \cup \ S \ \cdot \in) \cdot (outl \cup outr) \cdot cup^\circ &\\
= & \dtext{composition over join (reverse)} &\\
& (R \cup S) \ \cdot \in \cdot \ (outl \cup outr) \cdot cup^\circ &\\
\subseteq & \dtext{$(outl \cup outr) \cdot cup^\circ \subseteq \ \in\!\!\backslash\!\!\in$, division  cancellation~\eqref{law:div:cancel}} &\\
& (R \cup S) \ \cdot \in &
\end{derivation}
\qed
\end{proof}

\begin{corollary}\label{cor:sprout1}
  Taking $R = T \cdot dom\ T$, $S = id$ and observing that $luni \subseteq cup$, using lemma~\ref{lem:sprout1} we get that
\[
\in \cdot \ sprouts \ T \subseteq T \ \cdot \in \cup \in.
\]
\end{corollary}

Some of the newly sprouted partial solutions might not contribute to the complete solutions because they are already worse with respect to the others.  Since we wish to eliminate needless computation, we should prune the set of partial solutions as soon as possible.  To do this, we can make good use of the \emph{thinning} step discussed in section~\ref{sec:thinning}.

\subsection{Dynamic programming theorem}
\label{sec:dynam-progr-theor}

The basic variant of dynamic programming theorem for limits due to~\cite{Curtis96arelational} is as follows:

\begin{theorem}
\label{th:dyn-prog-theorem}
\index{dynamic programming!Dynamic programming theorem}
Let $M = min\, R \cdot \Lambda (lim\ T)$, $D \subseteq \ \in\!\!\backslash\!\!\in \cdot \ sprouts\ T$, where $R$ is a preorder on the set of completed solutions represented by $notdom\ T$, and the following conditions are satisfied:
\begin{align*}
  dom\ (T \, \cdot \in) \subseteq & \ dom\ D \\
  D\ \cdot \ni \cdot \ (lim\ T)^\circ \subseteq & \ni \cdot \ (lim\ T)^\circ \cdot R.
\end{align*}
Then
\[
min\, R \cdot lim\ D \cdot \tau \subseteq M.
\]
\end{theorem}

The theorem states that by repeatedly sprouting and thinning the partial solutions we will get a refinement of the naive approach of first generating all the solutions and then picking a minimum.

In the remainder of this section, we will prove a slightly extended version of the theorem.  Note that the theorem~\ref{th:dyn-prog-theorem} is a corollary of the following theorem when we set $P$ to $id$.

\begin{theorem}\label{th:dynth}
Let $M = min\, R \cdot \EF P \cdot \Lambda (lim\ T)$, where $P$ is a coreflexive relation such that $ran\ P \subseteq dom\ R$ and $R$ is a preorder.  Let the following conditions be satisfied:
\begin{align}
  dom\ (T\ \cdot \in) \subseteq & \ dom\ D \label{assum:dynth1} \\
  D \subseteq & \ \in\!\!\backslash\!\!\in \cdot \ sprouts\ T \label{assum:dynth2} \\
  D \ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ \subseteq & \ni \cdot \ (lim\ T)^\circ \cdot P^\circ \cdot R. \label{assum:dynth3}
\end{align}
Then
\[
min\, R \cdot \EF P \cdot lim\ D \cdot \tau \subseteq M.
\]
\end{theorem}

\begin{proof}
\begin{derivation}
& min\, R \cdot \EF P \cdot lim\ D \cdot \tau \subseteq M &\\
\equiv & \dtext{definition of M} &\\
& min\, R \cdot \EF P \cdot lim\ D \cdot \tau \subseteq min\, R \cdot \EF P \cdot \Lambda (lim\ T) &\\
\Leftarrow & \dtext{$\Lambda X = \EF X \cdot \tau$, monotonicity} &\\
& min\, R \cdot \EF P \cdot lim\ D \subseteq min\, R \cdot \EF P \cdot \EF  (lim\ T) &\\
\equiv & \dtext{let $N = min\, R \cdot \EF P \cdot \EF  (lim\ T)$} &\\
& min\, R \cdot \EF P \cdot lim\ D \subseteq N &\\
\equiv & \dtext{division~\ref{up:division:left}} &\\
& lim\ D \subseteq (min\, R \cdot \EF P) \backslash N &\\
\Leftarrow & \dtext{recursion equation for limits~\eqref{law:limits:recursion}} &\\
& notdom\ D \cup (min\, R \cdot \EF P) \backslash N \cdot D \subseteq (min\, R \cdot \EF P) \backslash N &\\
\equiv & \dtext{universal property of join~\ref{up:join}} &\\
& notdom\ D \subseteq (min\, R \cdot \EF P) \backslash N &\\
& \wedge ~~ (min\, R \cdot \EF P) \backslash N \cdot D \subseteq (min\, R \cdot \EF P) \backslash N&\\
\Leftarrow & \dtext{division, division cancellation~\eqref{law:div:cancel}} &\\
& min\, R \cdot \EF P \cdot notdom\ D \subseteq N &\\
& \wedge  ~~ N \cdot D \subseteq N&\\
\Leftarrow & \dtext{assumption~\eqref{assum:dynth1}, domains~\eqref{law:dom3}, expanded $N$, $\EF $ functor} &\\
& min\, R \cdot \EF P \cdot notdom\ (T\ \cdot \in) \subseteq min\, R \cdot \EF (P \cdot lim\ T) &\\
& \wedge ~~ min\, R \cdot \EF P \cdot \EF  (lim\ T) \cdot D \subseteq min\, R \cdot \EF (P \cdot lim\ T)&\\
\equiv & \dtext{univ. prop. of minimum~\ref{lemma:min-eximage}, univ. prop. of meet~\ref{up:meet}} &\\
& min\, R \cdot \EF P \cdot notdom\ (T\ \cdot \in) \subseteq P \cdot lim\ T\ \cdot \in \tageq\label{eq:dynth:1.1} &\\
& \wedge ~~ min\, R \cdot \EF P \cdot notdom\ (T\ \cdot \in) \ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ \subseteq R \tageq\label{eq:dynth:1.2} &\\
& \wedge ~~ min\, R \cdot \EF P \cdot \EF  (lim\ T) \cdot D \subseteq P \cdot lim\ T\ \cdot \in \tageq\label{eq:dynth:2.1} &\\
& \wedge ~~ min\, R \cdot \EF P \cdot \EF  (lim\ T) \cdot D \ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ \subseteq R \tageq\label{eq:dynth:2.2} &
\end{derivation}

Now we prove the four inequalities to complete the argument.  The first one can be justified as follows:
\begin{derivation}
  & min\, R \cdot \EF P \cdot notdom\ (T\ \cdot \in) & \\
  \subseteq & \dtext{definition of minimum~\ref{def:minimum}} & \\
  & \in \cdot \ \EF P \cdot notdom\ (T\ \cdot \in) & \\
  = & \dtext{property of existential image~\eqref{law:ex-image:natural}} & \\
  & P \, \cdot \in \cdot \ notdom\ (T\ \cdot \in) & \\
  \subseteq & \dtext{property of domains~\eqref{law:dom4}} & \\
  & P \cdot notdom\ T\ \cdot \in & \\
  \subseteq & \dtext{definition of limit~\ref{def:limit}, monotonicity} & \\
  & P \cdot lim\ T\ \cdot \in &
\end{derivation}

For the second, we argue:
\begin{derivation}
&min\, R \cdot \EF P \cdot notdom\ (T\ \cdot \in) \ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ &\\
\subseteq & \dtext{property of domains~\eqref{law:dom5}} &\\
&min\, R \cdot \EF P \ \cdot \ni \cdot \ notdom\ T \cdot (lim\ T)^\circ \cdot P^\circ &\\
= & \dtext{property of limits, \eqref{eq:notdom-lim}} &\\
&min\, R \cdot \EF P \ \cdot \ni \cdot \ notdom\ T \cdot P^\circ &\\
= & \dtext{coreflexives commute, $C = C^\circ$} &\\
&min\, R \cdot \EF P \ \cdot \ni \cdot \ P \cdot notdom\ T &\\
\subseteq & \dtext{for $X$ coreflexive: $\EF X \ \cdot \ni \cdot \ X \subseteq \ \ni$} &\\
&min\, R \ \cdot \ni \cdot \ notdom\ T &\\
= & \dtext{property of minimum} &\\
& R \cdot notdom\ T &\\
\subseteq & \dtext{property of coreflexives: $C \cdot D = C \cap D$} &\\
& R &
\end{derivation}

Third inequality can be proved thus:
\begin{derivation}
& min\, R \cdot \EF P \cdot \EF  (lim\ T) \cdot D &\\
\subseteq & \dtext{assumption~\eqref{assum:dynth2}} &\\
& min\, R \cdot \EF P \cdot \EF  (lim\ T)\ \cdot \in\!\!\backslash\!\!\in \cdot \ sprouts\ T &\\
\subseteq & \dtext{definition of minimum~\ref{def:minimum}, $\EF $ functor} &\\
& \in \cdot \ \EF (P \cdot lim\ T) \ \cdot \in\!\!\backslash\!\!\in \cdot \ sprouts\ T &\\
= & \dtext{property of existential image~\eqref{law:ex-image:natural}} &\\
& (P \cdot lim\ T) \ \cdot \in \cdot \in\!\!\backslash\!\!\in \cdot \ sprouts\ T &\\
\subseteq & \dtext{division cancellation~\eqref{law:div:cancel}} &\\
& P \cdot lim\ T \ \cdot \in \cdot \ sprouts\ T &\\
\subseteq & \dtext{property of sprouting, \ref{cor:sprout1}} &\\
& P \cdot lim\ T \cdot (T\ \cdot \in \cup \in) &\\
= & \dtext{union} &\\
& P \cdot lim\ T \cdot T\ \cdot \in \cup \ P \cdot lim\ T \ \cdot \in &\\
\subseteq & \dtext{property of limits} &\\
& P \cdot lim\ T\ \cdot \in &
\end{derivation}

\enlargethispage{-\baselineskip}
Finally, the fourth inequality is proved as follows:
\begin{derivation}
& min\, R \cdot \EF P \cdot \EF  (lim\ T) \cdot D \ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ &\\
\subseteq & \dtext{assumption~\eqref{assum:dynth3}} &\\
& min\, R \cdot \EF P \cdot \EF  (lim\ T)\ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P^\circ \cdot R &\\
= & \dtext{definition of existential image~\eqref{def:ex-image-with-pt}, converse} &\\
& min\, R \cdot \EF P \cdot \Lambda (lim\ T \ \cdot \in) \cdot (lim\ T \ \cdot \in)^\circ \cdot P^\circ \cdot R &\\
\subseteq & \dtext{power transpose cancellation, lemma \ref{lem:pt:cancel}, $C = C^\circ$} &\\
& min\, R \cdot \EF P \ \cdot \ni \cdot \ P \cdot R &\\
\subseteq & \dtext{for $X$ coreflexive: $\EF X \ \cdot \ni \cdot \ X \subseteq \ \ni$} &\\
& min\, R \ \cdot \ni \cdot \ R &\\
\subseteq & \dtext{property of minimum, $R$ preorder $\Rightarrow$ transitive} &\\
& R &
\end{derivation}

\qed
\end{proof}

This theorem allows us to generate partial solutions that are not admissible.  The relation $\EF P$ where $P$ is a coreflexive can be interpreted as a ``filter'' relation, retaining only the results that are comparable using $R$, the relation used to compare complete solutions.  This is useful if the generating process depends on values of some partial solutions that might not in the end produce an admissible result.

\section{The single-pair shortest paths problem}

In this section we will demonstrate the use theorem~\ref{th:dynth}.  The following is a well-known problem from graph theory where the goal is to find the shortest path from a source to a destination.  We can formalize the problem as follows:

\begin{problemDescription}
  In a \emph{single-pair shortest paths problem} we are given a weighted, directed graph $G = (V, E)$, with \emph{weight\footnote{We will also use the term \emph{length} interchangeably.}} function $w : E \rightarrow R$ mapping edges to real-valued weights.  The weight of path $p = v_0, v_1, \ldots , v_k$ is the sum of the weights of its constituent edges:
\[
\tilde w(p) = \sum_{i=1}^k w(v_{i-1}, v_i).
\]
We define the \emph{shortest path weight} from $u$ to $v$ by:
\[
\delta(u,v) =
\begin{cases}
\min \{\tilde w(p) \st u \overset{p}{\leadsto} v \} & \text{if path from $u$ to $v$ exists} \\
\infty & \text{otherwise}
\end{cases}
\]
The \emph{shortest path} from vertex $u$ to $v$ is then defined as any path $p$ with weight $\tilde w(p) = \delta(u,v)$.

The \emph{single-pair shortest paths problem} is the problem of finding the shortest path from source vertex $s$ to target vertex $t$.
\end{problemDescription}

In the following section, however, we will narrow the problem to a smaller domain.  Namely, only the graphs where the \emph{weight} function maps to the real non-negative numbers will be considered.  Using theorem~\ref{th:dynth} we will try to derive an efficient algorithm to solve it.

\paragraph{Specification}

The graph will be represented as a set of triples $(u,v,d)$ where $u$ represents the source vertex, $v$ the target vertex and $d \geq 0$ is the distance from $u$ to $v$.  A path in the graph will be represented simply as a cons list of visited vertices.  Note that we will only consider the \emph{simple paths}, that is paths with no repeated vertices.  This obviously isn't a restriction because any loop would only increase the path length and thus not contribute to the solution.

Since it is desired to find the shortest possible path, the relation to compare the solutions is one that compares on the path length.  The path length can be expressed as a catamorphism on non-empty lists
\[
plen = outl \cdot \cata{\langle id, zero \rangle, \langle outl, plus \cdot (w \times id) \cdot assocl \rangle},
\]
where the input is list of vertices comprising the path.  Then the relation $R$ to compare two paths can be defined as $R = plen^\circ \cdot leq \cdot plen$.

During the run of the algorithm, the solutions will be built by taking partially completed paths and adding edges to them.  The step relation can be thus defined as:
\begin{alignat*}{3}
(n \cons p, F \setminus \{n\}) ~&~ T ~&~ (p, F) &~~~ \text{if $head\ p \neq t \wedge (head\ p, n) \in E \wedge n \in F$} \\
(p, \emptyset) ~ & ~ T ~ & ~ (p, F) &
\end{alignat*}
where $p$ is a partial path (its head is the last added vertex), $n$ is the vertex added to the end of the path and $F$ is the set of vertices not yet used.  The input is a tuple $([s],V \setminus \{s\})$, where $s$ is the source vertex.

The relation $T$ will generate all maximal paths\footnote{That is paths that can't be extended anymore, not necessarily the longest paths.} in the graph starting in $s$, but will not continue extending a path if the target vertex was already reached.  Generating all paths is necessary because we do not know beforehand what path will lead to the target vertex and therefore we are required to expand all the paths until the whole graph is explored.  It also wouldn't suffice to stop at the moment when the algorithm first encounters the target, because there might be a longer path---in the hop count---that is better with respect to the weights.

Because of this, after the main loop is finished, some of the generated solutions are not admissible because they might not end in the target vertex.  We will therefore use the relation $P$ from theorem~\ref{th:dynth} to filter the resulting set and only retain the paths ending in the target vertex.  The relation can be defined using currying:
\[
(P\ t)\ x ~~ \equiv ~~ t = head\ x.
\]

Using these relations, we can then specify the problem of finding the shortest path from $s$ to $t$ as
\[
min\, R \cdot \EF (P\ t) \cdot \Lambda (lim\ T).
\]

\paragraph{Derivation}

To solve this problem with dynamic programming and thinning, we need a comparison relation $S$ which compares the partial solutions---that is the paths that can still be extended.  It is a simple observation---using the assumption that the weights are non-negative---that if a path $p$ from $x$ to $y$ is part of the shortest path $r$ from $x$ to $z$, then $p$ must also be the shortest path from $x$ to $y$.  If not, it would be a direct contradiction to the optimality of path $r$.  We should therefore keep the shortest path ending in each explored vertex, the other paths can be safely discarded.  This is captured in the preorder $S$:
\[
S = (head^\circ \cdot eq \cdot head) \cap (plen^\circ \cdot leq \cdot plen).
\]
Using relation $S$ we can define the thinning step simply as $thin\ S$.  Since the thinning step keeps exactly one path for each discovered vertex---the shortest one---sprouting all partial paths at once might produce many redundant expansions.  For example, if two paths can expand into the same vertex, it is guaranteed only one of them will be kept in the next step.  For this reason the sprouting step should  expand only one partial path to all the neighboring vertices.  The dynamic programming step is
\[
D = thin\ S \cdot sprout\ T.
\]

We can now check the conditions of the theorem~\ref{th:dynth} to see if a dynamic programming algorithm exists.  For the first, using the definition of $D$ above and expanding, we obtain
\begin{equation}\label{eq:cond1}
dom\ (T\ \cdot \in) \subseteq & dom\ (thin\ S \cdot cup \cdot (\Lambda T \cdot dom\ T \times id) \cdot lcons^\circ).
\end{equation}
In words, at any time when there is at least one uncompleted partial solution in our set, it is also possible to use $D$ on it.

Suppose the input set is not empty so the $\in$ relation won't fail.  Let $a$ be the element that gets picked and to which the relation $T$ is applied.  It then follows that $(a,a) \in dom\ T$ and the sprouting step is applicable.  This means that this step will return at least one partial solution.  In the edge case when the input set is just $\{a\}$, it will be precisely the set $\Lambda T\ a$.  The relation $thin\ S$ by definition keeps one path for each vertex and so will keep at least one path overall.  This justifies condition~\eqref{eq:cond1}.

The second condition is:
\begin{equation}\label{eq:cond2}
D\ \cdot \ni \cdot \ (lim\ T)^\circ \cdot P \subseteq \ \ni \cdot \ (lim\ T)^\circ \cdot P \cdot R
\end{equation}
Let $M = \ \ni \cdot \ (lim\ T)^\circ \cdot P$.  The relation $M$ produces one admissible solution by choosing one partial solution from the current set and expanding it.  In words, the condition states that when we apply $D$ to a set of partial solutions, then completing a path from this set will yield no worse result than completing a path from the original set of partial solutions.

We can represent the situation as the diagram
\[
\overline{Paths} \llongleftarrow{D} Paths \llongrightarrow{M} a \concat p, p \in Paths,
\]
where $Paths$ is the set of all partial solutions, $p$ is a path starting in $s$ and $a$ is some extension of it ending in $t$.

Then we  have to show that there exist $b$ and $\bar p$ such that:
\[
\overline{Paths} \llongrightarrow{M} b \concat \bar p, \bar p \in \overline{Paths} \llongleftarrow{R} a \concat p, p \in Paths.
\]
Let $x$ denote the vertex where $p$ ends.  We start by observing that if $p$ is in $\overline{Paths}$ then $p$ is the so far optimal path to $x$.  This is guaranteed by the thinning step where only the best path to each explored vertex is preserved.  In this case we can simply take $b = a$, $\bar p = p$.

Otherwise, the path to $x$ must have been improved or there was a better path to $x$ already present.  Let's denote this better path $\hat p$.  Then taking $b = a$, $\bar p = \hat p$ satisfies the conditions and~\eqref{eq:cond2} is proved.

Together, the conditions of theorem~\ref{th:dynth} ensures the algorithm is correct.  In the language of relations, it can be expressed as:
\begin{derivation}
& min\, R \cdot \EF (P\, t) \cdot lim\ (thin\, S \cdot cup \cdot (\Lambda T \cdot dom\, T \times id) \cdot lcons^\circ) \cdot \tau &\\
= & \dtext{$thin\ S$ keeps one path per vertex} &\\
& \EF (P\ t) \cdot lim\ (thin\ S \cdot cup \cdot (\Lambda T \cdot dom\ T \times id) \cdot lcons^\circ) \cdot \tau &
\end{derivation}
This algorithm is still non-deterministic because of the choice $lcons^\circ$ makes.  Since the correctness of the algorithm does not depend on the order in which we sprout the partial results, we can fixate some order and always pick the head of the list ordered by this routine.

The natural choice is to keep the list ordered by the length of the partial paths and in each step pick the shortest.  The justification is that if we expand vertex $x$ on some longer path first, it might happen that later we will discover a shorter path to $x$.   This would invalidate all the expansions from previous step, because we can now extend the shorter path to obtain better overall results for all the reached vertices.

We can therefore refine $lcons^\circ$ into a function that will pick the first partial solution that does not yet end in $t$ and still has some vertices that can be expanded.  This directly correspond to the condition $dom\, T$ in the $sprout$ step.

With this choice of ordering, the algorithm strongly resembles the well-known Dijkstra's algorithm~\cite{EWD:NumerMath59} for finding the shortest path.  Indeed, Dijkstra's algorithm uses the same properties of the underlying problem, namely the principle of optimality and so the results ought to be similar.

\paragraph{The program}

The partial solutions will be represented as triples (partial path, length, free vertices) to avoid recomputing the path length.  The functions that require the graph structure are parametrized by \textsc{graph} instead of taking it as an argument for clarity.

The program makes use of the \textsc{Maybe} monad to simulate the situation when relation isn't defined (akin to partial functions).

In the resulting Haskell program we represent all the state by lists.  This is not the most efficient choice.  The functions that operate on the graph (\textsc{isEdge}, \textsc{edgelen}, $\ldots$) can be replaced with more efficient variants, using e.g. maps and priority queues, if the need arises.

\programListing[43]{shortestpath}{Shortest path program}

Finally, let us briefly discuss the run time complexity of the algorithm.

The function \textsc{extend} extends the current path by appending vertices from the set $free$.  Since this set is bound by the size of $V$, this function runs in $\mathcal{O}(|V|)$.

The function \textsc{sprout} first finds a suitable vertex to expand among all the partial solutions.  Because we only keep one solution per vertex, this is again bound by $|V|$.  After a vertex is picked, \textsc{extend} is called, taking linear time.  Therefore, the \textsc{sprout} step runs in $\mathcal{O}(2|V|)$.

The \textsc{thin} step runs in quadratic time due to naive implementation of \textsc{nubBy} that eliminates the repeated occurrences of each end vertex.  However, because we can have at most two paths ending in same vertex at any time during the run of the algorithm, \textsc{thin} can be implemented in linear time with random memory access---simply look up the current vertex in an array, and if the solution is not yet present or the current one is better, update the data structure accordingly.

Therefore, \textsc{step} runs in quadratic time.  Because in each step we ``fi\-na\-li\-ze'' one vertex, \textsc{step} will be called at most $|V|$ times.  The total runtime of implementation shown in listing 5.1 is $\mathcal{O}(|V|^3)$, however, with better data structures, run time in $\mathcal{O}(|V|^2)$ can be achieved by rather trivial modification.  We have presented implementation using lists without introducing more complex data structures for increased clarity.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% reftex-default-bibliography: ("./bibliography")
%%% End: