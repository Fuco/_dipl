\section{Thinning}
\label{sec:thinning}

In the previous section, we have seen an application of the greedy approach to solving an optimization problem.  The greedy approach keeps only the best partial solution at each step towards the complete solution.  At the other extreme is the naive approach, when all the solutions are retained throughout the entire generation process, and only after all the solutions are generated the best one is selected.  In the middle of these lies the third possibility: at each step keep only an appropriate subset of the solutions, namely those that still have a chance to become the best complete solution.  Such algorithms are called \emph{thinning} algorithms.

We can achieve this by introducing a thinning step into the algorithm to eliminate needless computation.  The exact way on how the thinning is used depends on the particular way the solutions are generated.  We will encounter two uses, one in section~\ref{sec:dynamic-programming} and one in section~\ref{sec:dynam-progr-theor}, both concerning dynamic programming.  The thinning itself is performed by the relation $thin$ parametrized with preorder $S$.

\begin{definition}\index{thinning}
Let $S$ be a preorder, not necessarily connected.  Then:
\[
thin\, S = \ \in\!\!\backslash\!\!\in \cap \ (\ni \cdot \ S) \slash\!\!\ni
\]
\end{definition}

A subset of the input that is returned satisfies the property that every element in the input set has a lower bound (with respect to $S$) in the returned subset.  Since the relation $S$ does not have to be a connected preorder, two solutions might not always be mutually comparable.  This is why the relation $thin\, S$ returns a subset and not a minimum.

\begin{remark}
Notice that if the relation $thin\, S$ always returns a single solution what we get is a \emph{greedy algorithm}.  This shows that the greedy algorithms are only a special case of this class of thinning algorithms.  Conversely, if we let $S = id$, the thinning step will not remove any solutions and we will get the naive approach.
\end{remark}

\section{Dynamic programming}
\label{sec:dynamic-programming}\index{dynamic programming}

\emph{Dynamic programming} is an algorithmic method in which a problem is solved by identifying a collection of subproblems and solving them one by one, smaller first, using the results obtained from smaller instances to help compute the larger ones, until the entire problem is solved~\cite{DasguptaPapadimitriouVazirani200609}.

This crucial observation is called \emph{principle of optimality}~\cite{Bellman:1957}, which says that the complete solution can be obtained from the optimal solutions of subproblems.  Of course, not every problem does adhere to this principle, and the main difficulty in applying dynamic programming is identifying this optimal substructure.  If this principle is satisfied, one can then decompose the problem into subproblems in all possible ways, recursively solve the subproblems and then assemble an optimal solution from the partial results.  This is captured in theorem~\ref{th:dynamic}.

If the subproblems overlap, we can go a step further and introduce a scheme to remove the repeated computation.  The two main approaches are \emph{memoization} and \emph{tabulation}.  In short, memoization is a top-down scheme; the computed results for each subproblem are stored after the first computation and retrieved for subsequent use.  We will not explore this paradigm here, more information can be found for example in~\cite{DasguptaPapadimitriouVazirani200609}.  However, in section~\ref{sec:change-making-probl} we will make good use of tabulation, which is a bottom-up approach: the subproblems are solved in order of dependency and the results are stored in a table which is used to combine them for subsequent use.

We now formulate the \emph{Dynamic programming theorem} for problems specified by a hylomorphism.

\begin{theorem}[Dyn. prog. theorem, \cite{BirddeMoor96:Algebra}]
\label{th:dynamic}
\index{dynamic programming!Dynamic programming theorem}
  Let $h$ and $T$ be $\FF$-algebras where $h$ is a function, and $H = \cata{h} \cdot \cata{T}^\circ$.  Further, let $M = min\, R \cdot \Lambda H$.  If $h$ is monotonic on $R$ and $Q$ is a preorder satisfying $h \cdot \FF H \cdot Q^\circ \subseteq R^\circ \cdot h \cdot \FF H$, then
\[
(\mu X : min\, R \cdot \PF (h \cdot \FF X) \cdot thin\, Q \cdot \Lambda T^\circ) \subseteq M.
\]
\end{theorem}

\section{Change making problem}
\label{sec:change-making-probl}

The following problem is taken from~\cite{Martello:1990:KPA:98124} and is one of the many variants of \emph{Knapsack problem}.  We will use it to illustrate the application of the theorem~\ref{th:dynamic}.

\begin{problemDescription}
  Given $n \in \mathbb{N}$ \emph{item types} and a \emph{knapsack}, with
  \begin{align*}
    w_j \in \mathbb{N}^+ &= \text{\emph{weight} of an item of type}\ j \\
    c \in \mathbb{N}^+ &= \text{\emph{capacity} of the knapsack},
  \end{align*}
  select a number $x_j, \, j \in \{1, \ldots, n\}$ of items of each type so that the total weight is $c$ and the total number of items is a minimum, i.e.
  \begin{alignat}{4}
    &\text{minimize}  ~ & z = &\sum_{j=1}^n x_j &\label{prob:minimize}\\
    &\text{subject to} ~ & \    &\sum_{j=1}^n w_j x_j = c &\\
    & \ & \ & x_j \geq 0, \, x_j \in \mathbb{N}, j \in \{1, \ldots, n\}.&
  \end{alignat}
  The problem is called ``change-making'' since it can be interpreted as that of a cashier having to assemble a given change, $c$, using the least number of coins of specified values $w_j$ in the case where, for each value, an unlimited number of coins is available.  Making use of this analogy, we will refer to weights as \emph{denominations} and to the selected items as \emph{coins}.  The vector of denominations will be denoted $W$ and the vector of selected coins with $X$.
\end{problemDescription}

The problem is NP-hard~\cite{LuekerIntegerProg} so unless P=NP we have no prospect of finding an effective algorithm.  However, we can still derive \emph{pseudo-polynomial}\footnote{That is, polynomial with respect to the input \emph{value} $c$, not input \emph{length}, which is approximately $\log c$.} algorithm using dynamic programming.

First, we will make two observations worth noting.  The first one is that for certain choices of denominations the problem can be solved by a greedy algorithm in linear time, when we simply take the largest possible value from the remainder each time.  In this case, the denominations vector is called \emph{canonical} and the conditions for testing whether a vector is canonical are given in~\cite{Chang:1970:ASC:321556.321567}.  For example, the commonly used denomination series for change $\langle 1, 2, 5, 10, 20, 50\rangle$ form a canonical vector.  However, we can easily construct a non-canonical vector, for example $\langle 1,7,8\rangle$.  Indeed, if we try to make change for $14$, a greedy algorithm would end up with $8, 1,\ldots, 1$ while the optimal solution is $7, 7$.

The second one is that for a particular choice of denominations a solution might not exist at all.  As an example, consider $W = \langle 4,5\rangle$ and $c = 7$.  To make the following derivation easier and cleaner, we will assume there is always a coin with weight 1.  This assumption does not make the problem any easier to solve, but eliminates handling of special cases when feasible solution doesn't exist.  The resulting program can then be easily extended to handle this case with a simple test.

\paragraph{Specification}

To make use of theorem~\ref{th:dynamic}, we will respecify the problem in the language of relations.  Even though the problem description only talks about minimizing the number of coins used~\eqref{prob:minimize}, the result of which coins were actually used is probably even more interesting.  We will therefore try to define relation $makechange$ that will split a given sum of money $c$ into all possible lists of coins whose weights will sum up back to $c$, and pick one with minimal length as the solution.

To encode the constraints on the available denominations, we define relation $split$ that takes a number $d$ and splits it into a pair $(w,d')$, where $w \in W$ is a valid denomination and $d' \geq 0$ is the remainder to be split.  That is, the relation $split : (Nat, Nat) \leftarrow Nat$ is defined as:
\[
(w, d')\ split \ d ~ \Leftrightarrow ~ w \in W,\, d' \geq 0,\, w+d' = d.
\]
The relation $sum$ defined as catamorphism
\[
sum = \cata{zero, split^\circ}
\]
sums a list of numbers, where each number is a valid denomination $w_j \in W$.  The specification of the problem in the form of hylomorphism is then
\begin{align}
  makechange = min\, R \cdot \Lambda (\cata{\alpha} \cdot sum^\circ),
\end{align}
where $R = length^\circ \cdot leq \cdot length$ and $\alpha = [nil, cons]$ is the initial algebra for the functor $\FF_AX = 1 + A \times X$ of lists. By refining this relation to a function $mc \subseteq makechange$, we will obtain the program specification.

\paragraph{Derivation}

To apply theorem~\ref{th:dynamic} we first have to show that $\alpha$ is monotonic on $R$, i.e.
\begin{align}
  \alpha \cdot \FF R \subseteq R \cdot \alpha. \label{eq:change:mono}
\end{align}
Let $k = [zero, succ \cdot outr]$.  Clearly,
\begin{align}
length \cdot \alpha = k \cdot \FF length. \label{eq:change:monoder1}
\end{align}
From the definition of $R$ and shunting~\eqref{law:fun:shunting1} we obtain
\begin{align}
  length \cdot R &\subseteq leq \cdot length. \label{eq:change:monoder2}
\end{align}

We argue:
\begin{derivation}
  & \alpha \cdot \FF R \subseteq R \cdot \alpha &\\
\equiv & \dtext{definition of R, shunting \eqref{law:fun:shunting1}} &\\
  & length \cdot \alpha \cdot \FF R \subseteq leq \cdot length \cdot \alpha &\\
\equiv & \dtext{assumption on $k$ \eqref{eq:change:monoder1}, $\FF$ functor} &\\
  & k \cdot \FF (length \cdot R) \subseteq leq \cdot k \cdot \FF length &\\
\Leftarrow & \dtext{definition of R, \eqref{eq:change:monoder2}} &\\
  & k \cdot \FF (leq \cdot length) \subseteq leq \cdot k \cdot \FF length &\\
\Leftarrow & \dtext{functors} &\\
  & k \cdot \FF leq \subseteq leq \cdot k &\\
\Leftarrow & \dtext{$k$ monotonic on $leq$} &\\
  & true &
\end{derivation}
The fact that $k$ is monotonic on $leq$ is very easy to verify and we leave it as an exercise to the reader.

During the decomposition step $\Lambda [zero^\circ, split]$, it can happen that one of the subproblems will end up with a remainder of 0.  It is clear that all the other solutions generated in this step will be worse, because each further decomposition using $split$ adds one coin to the list.  We can therefore introduce a thinning step that, in case when some solution has a remainder of 0, discards all the other solutions.  A relation $Q$ realizing such ordering can be defined as ``flat'' preorder
\[
(u, 0) &\leq (v,d) ~~~ \forall d \geq 0\,;\ u, v \in W. \\
\]
However, this definition is cumbersome to use in subsequent calculations.  We can define $Q$ in the terms of calculus of relations as $Q = \FF (\Pi, lez)$ where $lez$ is the smallest preorder containing $\{(0,d) \st \forall d \geq 0,\,d \in \bathbb{N} \}$.

We can now formulate the second condition from theorem \ref{th:dynamic} to introduce the thinning step.  After substituting the actual relations, we get:
\[
\alpha \cdot \FF sum^\circ \cdot Q^\circ \subseteq R^\circ \cdot \alpha \cdot \FF sum^\circ.
\]
Before the main derivation, we will need to justify two supporting claims.  The first claim is:
\begin{align}
  sum^\circ \cdot lez^\circ &\subseteq R^\circ \cdot sum^\circ . \label{eq:change:thinder1}
\end{align}
Since converse is monotonic with respect to inclusion, this is equivalent to showing that $lez \cdot sum \subseteq sum \cdot R$.  Representing the inequality as diagram, we get
\[
a \llongleftarrow{lez} x \llongleftarrow{sum} b \Rightarrow a \llongleftarrow {sum} y \llongleftarrow{R} b.
\]
Now, only two possible situations can occur.  If $a = x$, then because $R$ is reflexive, we get at once that $x = y$ satisfies the implication.  For the other case, $a = 0$, $x \geq 0$ and $b$ is a list whose sum is $x$.  Again, it is easy to see that taking $y = []$ will satisfy the condition.

The other claim is that
\begin{align}
  \alpha \cdot \FF (\Pi, R) &\subseteq R \cdot \alpha \label{eq:change:thinder2}
\end{align}
However, this is immediately obvious from~\eqref{eq:change:mono}, the monotonicity of $\alpha$ on $R$.  The main argument is now as follows:
\begin{derivation}
  & \alpha \cdot \FF (id, sum^\circ) \cdot Q^\circ &\\
= & \dtext{definition of Q, bifunctors, converse} &\\
  & \alpha \cdot \FF (\Pi, sum^\circ \cdot lez^\circ) &\\
\subseteq & \dtext{claim \eqref{eq:change:thinder1}} &\\
  & \alpha \cdot \FF (\Pi, R^\circ \cdot sum^\circ) &\\
\subseteq & \dtext{bifunctors} &\\
  & \alpha \cdot \FF (\Pi, R^\circ) \cdot \FF (id, sum^\circ) &\\
\subseteq & \dtext{claim \eqref{eq:change:thinder2}, converse, shunting} &\\
  & R^\circ \cdot \alpha \cdot \FF (id, sum^\circ) &
\end{derivation}

With both conditions satisfied, we can finally apply the Dynamic programming theorem; the optimal change can be obtained as the least fix point of the recursive equation
\[
X = min\, R \cdot \PF [nil, cons \cdot (id \times X)] \cdot thin\, (\Pi + (\Pi \times lez)) \cdot \Lambda [zero^\circ, split].
\]
Because $split$ will never return an empty list, the ranges of $split$ and $zero^\circ$ are disjoint and we can rewrite the equation using a conditional statement $(p \rightarrow t, e)$ which corresponds to familiar conditional statement from functional programming.  We obtain
\[
X = (zp \rightarrow nil, min\, R \cdot \PF (cons \cdot (id \times X)) \cdot thin\, (\Pi \times lez) \cdot \Lambda split),
\]
where $zp$ is a predicate testing for zero: $zp\ d \Leftrightarrow d = 0$.

We can merge the thinning and generating steps together and implement $thin\, (\Pi \times lez) \cdot \Lambda split$ as function $thinspl : list\, (Nat \times Nat) \leftarrow Nat$.  The function would generate all the possible splits, using the highest denomination first.  Because denominations do not repeat, the situation when remainder is zero can occur in only one split.  Thus, if the first valid split has remainder zero, we return just this singleton list, otherwise we compute all the splits and return the entire list.  This gives us
\begin{align}
  mc = (zp \rightarrow nil, minlist\, R \cdot map\, (cons \cdot (id \times mc)) \cdot thinspl).\label{prog:mc}
\end{align}
Since $minlist\, R = \cata{id, bmin\, R}$ and $map\, f = \cata{\alpha \cdot \FF (f, id)}$ we can apply fusion law from lemma~\ref{law:fusion-cata}.  Let $h = (cons \cdot (id \times mc))$ and $m = [id, bmin\, R]$.  Then we argue:
\begin{derivation}
  & \cata{m} \cdot \cata{\alpha \cdot \FF (h, id)} = \cata{m \cdot \FF (h, id)} &\\
\Leftarrow & \dtext{fusion~\ref{law:fusion-cata}} &\\
  & \cata{m} \cdot \alpha \cdot \FF (h, id) = m \cdot \FF (h, id) \cdot \FF (id, \cata{m}) &\\
\equiv & \dtext{universal property of catamorphism~\eqref{up:cata}} &\\
  & m \cdot \FF (id, \cata{m}) \cdot \FF (h, id) = m \cdot \FF (h, id) \cdot \FF (id, \cata{m}) &\\
\equiv & \dtext{$\FF$ bifunctor} &\\
  & true &
\end{derivation}

In the end, $mc$ is implemented by the program
\begin{align}
  mc = (zp \rightarrow nil, \cata{bmax\, R \cdot (cons \cdot (id \times mc) \times id)} \cdot thinspl).\label{prog:mc-final}
\end{align}


The program terminates because the second element of tuples returned by $thinspl$ is always strictly less than the input.  This is guaranteed by the fact that coins with value 0 are not allowed.

The problem with this implementation is that the running time is exponential in the input $c$.  The reason is that many subproblems are calculated many times over.  This is where the aforementioned tabulation scheme comes into play.  By solving the subproblems in appropriate order and reusing the previously computed values, we can bring the runtime from exponential to $\mathcal{O}(cn)$ where $n$ is the number of denominations.

\paragraph{Tabulation}
Focusing only on the size of the solution, we can rewrite the equation~\eqref{prog:mc} as
\[
mc\ k =
\begin{cases}
  1 + \min \{mc\, (k - w_j) \st \forall w_j \in W \} & k > 0 \\
  0 & k = 0 \\
  \infty & \text{otherwise}
\end{cases}
\]

We start by observing that to compute $mc\ k$, we also need to compute $mc\, (k - w_j)$ for all $w_j \in W$.  Figure~\ref{fig:principle-of-opt} illustrates the situation for weight vector $W = \langle 1, 7, 8 \rangle$; the optimal solution for $k$ is the best of the three with the respective coin added to the pile.

\begin{figure}
\centering
\begin{tikzpicture}[every node/.style={block},
        block/.style={minimum height=1.5em,outer sep=0pt,draw,rectangle,node distance=0pt,minimum width=1.2cm}]
   % \draw[help lines] (0,0) grid (10,1);
   \node (A) {$k$};
   \node (B) [right=of A] {$k-1$};
   \node (C) [right=of B] {$k-2$};
   \node (D) [right=of C] {$~~~~\dots~~~~$};
   \node (E) [right=of D] {$k-7$};
   \node (F) [right=of E] {$k-8$};
   \node (G) [right=of F] {$k-9$};
   \node (H) [draw=white, right=of G] {$~~\dots$};
   \draw (G.south east) -- ++(0,1.5em);
   \draw (G.north east) -- ++(2cm,0) (G.south east) -- ++ (2cm,0);
   \path[-latex]
   (A.north) edge [bend left=25] (B.north)
   (A.north) edge [bend left=25] (E.north)
   (A.north) edge [bend left=25] (F.north);
\end{tikzpicture}
\caption{The principle of optimality.}
\label{fig:principle-of-opt}
\end{figure}

This suggests that if we start building the table from $k = 1$, we will never have to repeat any computation twice.  For reasons of efficiency, we will save each partial solution as a pair $(m,ds)$ where $ds$ is list of the used coins and $m$ is its length.

To match the format of the table, we will represent the denominations vector as ``bit string'', with $(1, w_j)$ at each position $w_j \in W$ of some valid denomination, and $(\infty, i)$ otherwise.  For example, the denominations vector $W$ from earlier would transform into
\[
[(1, 1), (\infty, 2), \ldots, (\infty, 6), (1, 7), (1, 8)].
\]
The first element of the pair represents the increment of length of the solution, the second is the added coin.  An increment of infinity represents invalid denomination.  We will use this vector and the table of partial results to compute each successive solution.

We will aim to express the function $mc$ as a catamorphism
\[
mc = head \cdot \cata{base, step},
\]
where $head$ simply returns the first element of the resulting list---the table of partial results.

Before we can define the function $step$, we will need to present some auxiliary functions.  Let $\LF$ denote the list functor and $\NF$ the pair functor.  The function \index{zip} $zip : \LF \NF (A, B) \leftarrow \NF (\LF A, \LF B)$ then commutes them together, where the resulting list has the length of the shorter one.
Function $\oplus$ defined as
\[
(n, d) \oplus (m, ds) = (n+m, d \cons ds)
\]
combines one element from the denominations vector and one from the table.  Each successive solution can then be computed by properly aligning the two data structures using the function
\[
step = cons \cdot \spl{minlist\, S \cdot map\,\oplus \cdot \, zip}{outr}.
\]
Since we keep the length of each list precomputed in the first element of the pair, the relation $S$ to compare the solutions is $S = outl^\circ \cdot leq \cdot outl$.

The process is illustrated on figure~\ref{fig:step-function}.  On the top is the table of partial results\footnote{The lists of coins used for each split are omitted for clarity.} and on the bottom is the denominations vector.

\begin{figure}
\centering
\begin{tikzpicture}[every node/.style={block},
        block/.style={minimum height=1.5em,minimum width=1.3cm,
          outer sep=0pt,draw,rectangle,
          node distance=1.5cm and 0pt},
        descr/.style={fill=white,inner sep=2.5pt, draw=white},
        sumrec/.style={rectangle,rounded corners,draw=black}]
   \node (A) {$k \leadsto 4$};
   \node (B) [right=of A] {$4$};
   \node (C) [right=of B] {$5$};
   \node (D) [right=of C] {$~~~~\dots~~~~$};
   \node (E) [right=of D] {$4$};
   \node (F) [right=of E] {$3$};
   \node (G) [right=of F] {$4$};
   \node (H) [draw=white, right=of G] {$~~\dots$};

   \node (J) [below=of B] {$1$};
   \node (K) [right=of J] {$\infty$};
   \node (L) [right=of K] {$~~~~\dots~~~~$};
   \node (M) [right=of L] {$\infty$};
   \node (N) [right=of M] {$1$};
   \node (O) [right=of N] {$1$};

   \draw (H.south west) -- ++(0,1.5em);
   \draw (H.north west) -- ++(2cm,0) (H.south west) -- ++ (2cm,0);

   \path[<->, >=stealth', shorten <=2pt, shorten >=2pt]
   (J.north) edge node[descr] {$+$} (B.south)
   (K.north) edge node[descr] {$+$} (C.south)
   (M.north) edge node[descr] {$+$} (E.south)
   (N.north) edge node[descr] {$+$} (F.south)
   (O.north) edge node[descr] {$+$} (G.south);

   \node (SUM) [sumrec, below=0.45cm of E, minimum width=7.8cm, xshift=-0.93cm] {$\ $};
   \path[->, >=stealth', shorten <=2pt, shorten >=2pt]
    (SUM.west) edge[bend left=45] node[auto, draw=none] {$min\, S$} (A.south);
\end{tikzpicture}
\caption{The step function.}
\label{fig:step-function}
\end{figure}

The program is then specified as
\[
mc\, (d,\, n) = head \cdot \cata{wrap \cdot (0,[]), step} \ (replicate\, n\, (encode\, d)),
\]
where $replicate\, n\, x$ returns list with $x$ repeated $n$ times and $encode$ performs the input denominations vector encoding.

\paragraph{The program}

Because of the way functional languages handle lists, the time complexity of the resulting program is actually higher than the announced $\mathcal{O}(cn)$, being $\mathcal{O}(c \max W)$.  The necessity of this is clear from the fact that to get to the solution of subproblem for $n - w_j$, we have to ``drop'' $w_j$ items from the list representing the table.

Because the catamorphism folds a list of only the same repeated element, it is clear that we can replace it by a simple loop using an accumulator parameter.  This will eliminate the need to pre-generate this list, which gets quickly very large for huge input instances.

The expression $1/0$ signifies infinity in Haskell, and is greater than any other number.  Infinity plus a finite number yields infinity again.  Function $map\, \oplus \cdot \, zip$ is replaced by standard Haskell function \textsc{zipWith}.  Function \textsc{encode} expects the input vector sorted in descending order.

\begin{figure}[ht!]
\programListing{change}{Change making program}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% reftex-default-bibliography: ("./bibliography")
%%% End: