\section{Binary relations}
\label{sec:relations}
\index{relation|(}

Equational reasoning in functional programming often employs functions as the basic units of expression.  In this thesis we will generalize from functions to relations.  The extension to relations provides us with more expressive power, where we can think of relations as mapping values to other \emph{sets} (possibly empty) of values in a non-deterministic fashion, where mappings to empty sets represent partial functions.

This allows us to specify problems in very concise manner.  For example to specify all possible splittings of a string we can simply say that $(x, y)$ and $z$ are related if $x \concat y \equiv z$.  Taking all the pairs related to $z$ gives us all the possible solutions.  Similarly, an optimization problem can be specified as taking minimum from a set of candidates without specifying the concrete implementation.

In this section we will give a brief overview of the definitions and operations on relations that will be used later.  This is by no means an exhaustive list, an interested reader can find more information in \cite{BirddeMoor96:Algebra} where a categorical approach to build the theory is chosen.

\subsection{Relations}
\label{sec:relation}
\index{relation!binary}

From now on we will always mean \emph{binary} relations when we talk about relations, unless otherwise specified, that is relations between two sets.

\begin{definition}
  A \emph{relation} $R : A \leftarrow B$ (we say ``to $A$ from $B$'') is a subset of cartesian product $A \times B$.  Elements $x$ and $y$ are \emph{related} if $(x, y) \in R$.  We will often write $xRy$ instead.
\end{definition}

We will also use the diagram notation, where the arrows will show the direction of ``mapping''.  For example, to express $xRy \wedge ySz$ we write
\[
x \llongleftarrow{R} y \llongleftarrow{S} z,
\]
and similarly for $xRy \wedge zSy$ we use
\[
x \llongleftarrow{R} y \llongrightarrow{S} z.
\]

Three special relations are the \emph{empty}, \emph{universal} and \emph{identity} relation.  These are defined as
\begin{align*}
\emptyset ~ &= ~ \{\ \} \\
\Pi_{A \times B} &= \{ (a,b) \st a \in A, b \in B \} \\
id_A ~ &= ~ \{(a,a) \st a \in A\}.
\end{align*}
The subscripts will be omitted when they are clear from context.

\subsection{Basic operations}
\label{sec:basic-operations}

As functions, two relations can be composed to form another relation.

\begin{definition}\index{relational!composition}\index{composition!of relations}
Let $R : A \leftarrow B$, $S : B \leftarrow C$ be two relations.  The \emph{composition} of $R$ after $S$ is the relation $R \cdot S : A \leftarrow C$ and is defined as
\[
R \cdot S = \{(a, c) \st \exists b ~.~ aRb \wedge bSc \}.
\]
\end{definition}

The composition of relations is associative and has the identity relation $id$ as identity.

Since relations are regular sets, we can naturally compare two relations with the same domain and codomain using set inclusion $\subseteq$.  Composition is monotonic with respect to this ordering, that is,
\[
(R_1 \subseteq R_2) \wedge (S_1 \subseteq S_2) \Rightarrow (R_1 \cdot S_1) \subseteq (R_2 \cdot S_2).
\]

\begin{definition}\index{relational!meet}
Let $R$, $S$ be two relations of type $A \leftarrow B$.  Then the relation $R \cap S : A \leftarrow B$ is called \emph{meet} of $R$ and $S$.  This relation is characterized by the universal property\index{universal property!of meet}
\[
\forall X : A \leftarrow B ~.~ (X \subseteq (R \cap S) ~ \equiv ~ (X \subseteq R) \wedge (X \subseteq S)).\label{up:meet}
\]
\end{definition}

On the point level, the meet of two relations is simply intersection of the corresponding sets.  The meet terminology comes from \emph{Order} theory and means the greatest lower bound of the two sets.

The meet operation is associative, commutative and idempotent and so forms a semilattice which induces an ordering on the set.  We could have therefore omitted the mention of $\subseteq$ as it can be showed that $R \subseteq S \equiv R \cap S = R$.  This means the algebraic and set-theoretic definitions of semilattice are equivalent and we can freely use the more convenient one in any given situation.

The composition semi-distributes over the meet:\index{distributivity!of composition over meet}
\begin{align*}
R \cdot (S \cap T) &\subseteq (R \cdot S) \cap (R \cdot T) \\
(R \cap S) \cdot T &\subseteq (R \cdot T) \cap (S \cdot T).
\end{align*}

\begin{definition}\index{relational!join}
Let $R$, $S$ be two relations of type $A \leftarrow B$.  Then the relation $R \cup S : A \leftarrow B$ is called \emph{join} of $R$ and $S$.  This relation is characterized by the universal property\index{universal property!of join}
\[
\forall X : A \leftarrow B ~.~ ((R \cup S) \subseteq X ~ \equiv ~ (R \subseteq X) \wedge (S \subseteq X)). \label{up:join}
\]
\end{definition}

On the point level, the join of two relations is the union of corresponding sets.   Since every meet semilattice is join semilattice under inverse order, all the basic properties of meet apply to join as well.

Unlike with meet however, composition distributes over join:\index{distributivity!of composition over join}
\begin{align*}
R \cdot (S \cup T) &= (R \cdot S) \cup (R \cdot T) \\
(R \cup S) \cdot T &= (R \cdot T) \cup (S \cdot T)
\end{align*}

Meet $\cap$ and join $\cup$ also distribute over each other.

\begin{definition}\index{relational!converse}
Let $R : A \leftarrow B$ be a relation.  Then the relation $R^\circ : B \leftarrow A$ called the \emph{converse} of $R$ is defined by
\[
R^\circ = \{ (y,x) \st (x,y) \in R \}.
\]
\end{definition}

Direct corollary of the definition is that converse operation is an \emph{involution}\index{involution}, that is $(R^\circ)^\circ = R$.  Other properties of converse are that it is order-preserving, contravariant and distributive over meet and join:
\begin{alignat}{2}
&R \subseteq S ~& &\equiv ~ R^\circ \subseteq S^\circ \label{law:conv:mono} \\
&(R \cdot S)^\circ ~& &= ~ S^\circ \cdot R^\circ \label{law:conv:contra}\index{contravariance} \\
&(R \cup S)^\circ ~& &= ~ R^\circ \cup S^\circ \\
&(R \cap S)^\circ ~& &= ~ R^\circ \cap S^\circ
\end{alignat}

As a special notation, we will write $\ni$ for $\in^\circ$, the set membership relation.

\subsection{Properties of relations}

We can classify relations with respect to mathematical functions, that is mappings that to each value assign precisely one image.  This will also show how functions are really just a special case of relations.

\begin{definition}
Let $S : A \leftarrow B$ be a relation.  We say that $S$ is
\begin{itemize}
\item simple if $S \cdot S^\circ \subseteq id_A$\index{simple relation}
\item total (entire) if $id_B \subseteq S^\circ \cdot S$\index{entire relation}
\item function if it is both simple and total.
\end{itemize}
\end{definition}

With functions, we get the \emph{shunting} laws:\index{shunting laws!for functions}
\begin{align}
  f \cdot R \subseteq S &\Leftrightarrow R \subseteq f^\circ \cdot S \label{law:fun:shunting1} \\
  R \subseteq S \cdot f &\Leftrightarrow R \cdot f^\circ \subseteq S
\end{align}

The following list contains some common properties of binary relations.  We will use the inequalities of the relational calculus instead of the usual point-wise definitions.

\begin{definition}
Let $R : A \leftarrow A$ be a relation.  We say that $R$ is
\begin{itemize}
\item \emph{reflexive} if $id_A \subseteq R$\index{reflexivity}
\item \emph{transitive} if $R \cdot R \subseteq R$\index{transitivity}
\item \emph{preorder} if it is both reflexive and transitive\index{preorder}
\item \emph{connected} if $R \cup R^\circ = \Pi$.\index{relation!connected}
\end{itemize}
\end{definition}

A converse of preorder is again a preorder, and so is a meet of two preorders.  We will use relations which are preorders to compare elements of sets---indeed the name preorder implies they are useful for ordering.  The last property of connectedness means that any two elements are comparable.  With a connected preorder we are therefore guaranteed to be able to take a minimum with respect to $R$.

Lastly, we will define a \emph{closure}\index{closure@closure, reflexive, transitive} operator that will be used in chapter~\ref{sec:limits} to model loops in computer programs.

\begin{definition}
The \emph{reflexive transitive closure} $R^*$ of $R$ is the smallest preorder containing $R$ as subset.
\end{definition}

The star notation is commonly used to mean $iteration$ (for example \emph{Kleene star} in automata theory) and we can interpret it here in a similar manner: we will relate all elements in $R$ that are required by transitivity until the relation doesn't change any more.  This means that any two elements related by a ``chain'' of related elements are also themselves related.  This justifies our intuition in viewing the closure operator as loop---the chain of related elements are the intermediate states before the loop termination.

\subsection{Coreflexives and filters}

As is often the case in category theory, the consideration of \emph{dual} structures is very useful.  We shall honor this tradition and introduce coreflexive relations.

\begin{definition}\index{coreflexivity}
A relation $R : A \leftarrow A$ is called \emph{coreflexive} if $C \subseteq id_A$
\end{definition}

One can intuitively view them as subsets or filters of a set.  For example, a coreflexive relation $even = \{ (n,n) \st n \in \mathbb{N}, n \equiv 0 \pmod 2 \}$ determines the subset of even natural numbers.  To view it as a filter, consider relation $even \cdot leq$.  Applied to $n$, this would return\footnote{The relation itself only relates one element to another.  By ``return'' we mean taking the range of the relation.  This will be made more precise in section~\ref{sec:power-transpose}.} a set of all even numbers less than or equal to $n$.

Using the intuition that coreflexives represent subsets, we will write $x \in C$ instead of the formally correct $(x,x) \in C$ for $C$ coreflexive.

Two very useful coreflexives are those representing the \emph{domain} and \emph{range} (or \emph{codomain}) of a relation.

\begin{definition}\index{range}\index{domain}\index{codomain}
  Let $R : A \leftarrow B$ be a relation.  Then the \emph{domain} of $R$ is represented by relation $dom\, R : B \leftarrow B$ and its \emph{range} is represented by relation $ran\, R : A \leftarrow A$.  These are defined as
\begin{align*}
dom\, R &= \{ (y,y) \st \exists x ~.~ (x, y) \in R \} \\
ran\, R &= \{ (x,x) \st \exists y ~.~ (x, y) \in R \}.
\end{align*}
\end{definition}

Similarly, relation $notdom\, R$\index{notdom@\textsl{notdom}} might be defined as those elements not in the domain of $R$:
\[
notdom\, R = \{ (y,y) \st \neg\exists x ~.~ (x,y) \in R \}.
\]

The following properties of domains and ranges will be useful:
\begin{align}
  R &= R \cdot dom\, R \\
  R &= ran\, R \cdot R \\
  dom\, R \subseteq dom\, S &\Leftrightarrow notdom\, S \subseteq notdom\, R \label{law:dom3} \\
  S \cdot notdom\, (R \cdot S) &\subseteq notdom\, R \cdot S \label{law:dom4} \\
  notdom\ (R \cdot S) \cdot S^\circ &\subseteq S^\circ \cdot notdom\ R \label{law:dom5}
\end{align}

First three properties are simple consequences of the definitions.  An informal proof of the fourth property will use the following diagram:
\[
(x \llongleftarrow{S} y \llongleftarrow {notdom\, (R \cdot S)} y) \Rightarrow (x \llongleftarrow{notdom\, R} x \llongleftarrow{S} y).
\]
Supposing $xSy$, to satisfy the condition $y \in notdom\, (R \cdot S)$ it must be the case that there does not exist any $z$ such that $zRx$.  But this is precisely the statement that $x \not\in dom\, R \Leftrightarrow x \in notdom\, R$.  Finally,~\eqref{law:dom5} is corollary of~\eqref{law:dom4}, after simple application of~\eqref{law:conv:mono} and~\eqref{law:conv:contra}.

\subsection{Implication and preorder composition}
\label{sec:impl-preord-comp}

\begin{definition}\index{implication}
Given two arrows $R, S : A \leftarrow B$, the \emph{implication} relation $(R \Rightarrow S) : A \leftarrow B$ is defined by the universal property\index{universal property!of implication}
\[
\forall X : A \leftarrow B ~.~ (X \subseteq (R \Rightarrow S) ~ \equiv ~ (X \cap R) \subseteq S).
\]
\end{definition}

The point interpretation is that $a\, (R \Rightarrow S)\, b \equiv (aRb \Rightarrow aSb)$.  We can use implication to define \emph{preorder composition}.

\begin{definition}\label{def:preorder-composition}\index{composition!of preorders}
Given two preorders $R$ and $S$, their \emph{preorder composition} is again a preorder and is defined as
\[
R ; S = R \cap (R^\circ \Rightarrow S).
\]
\end{definition}

The relation $R ; S$ first compares two elements by $R$, and if they are equivalent with respect to $R$, then compares them with $S$.

\subsection{Division}

The \emph{division} operation gives us a way to express universal quantification using relations.  It is equally useful in manipulation of equations, where it allows us to ``shuffle'' relations between the left and right side of the inequality.

\begin{definition}\index{division!left}
Given two arrows $R : A \leftarrow B$ and $S : A \leftarrow C$, the \emph{left-division} $R \backslash S : B \leftarrow C$ is defined by the universal property\index{universal property!of left division}
\[
X \subseteq R \backslash S ~\equiv~ R \cdot X \subseteq S, \label{up:division:left}
\]
that is, $X = R \backslash S$ is the largest relation that makes the following diagram semi-commute:
\begin{diagram}
  \matrix (m) [matrix of math nodes,row sep=0.7em,column sep=2.5em,minimum width=2em]
  {
      B &  & C \\
         & \subseteq & \\
         & A &\\};
  \path[-stealth]
    (m-1-1) edge node [auto, swap] {$R$} (m-3-2)
    (m-1-3) edge node [above] {$X$} (m-1-1)
                edge node [auto] {$S$} (m-3-2);
\end{diagram}
\end{definition}

We can interpret the relation $R \backslash S$ as
\[
b\, (R \backslash S)\, c ~ \equiv ~ (\forall a ~.~ aRb \Rightarrow aSc).
\]
\begin{example}
A \emph{subset} relation can be succinctly specified as $\in\!\!\backslash\!\!\in$.  If we denote $\in\!\!\backslash\!\!\in ~\equiv ~ \subseteq$, then following the diagram we get the statement
\[
a \in B \wedge B \subseteq C \Rightarrow a \in C,
\]
which obviously holds.
\end{example}

Analogically, we can define a \emph{right-division} operator by universal property\index{universal property!of right division}\index{division!right}
\[
X \subseteq S \slash R ~ \equiv ~ X \cdot R \subseteq S,
\]
which at point level reads
\[
c\, (S \slash R)\, b ~ \equiv ~ (\forall a ~.~ cSa \Leftarrow bRa).
\]
We can visualize this on the diagram above by simply reversing the arrows.  This also shows us that the left and right division are tightly connected and can be expressed in terms of each other.  Since the reversing of arrows corresponds to converse, we get: $R \backslash S = (S^\circ \slash R^\circ)^\circ$ and $S \slash R = (R^\circ \backslash S^\circ)^\circ$.

The cancellation laws are:\index{cancellation law!for division}
\begin{align}
  R \cdot (R \backslash S) &\subseteq S \label{law:div:cancel} \\
  (R \slash S) \cdot S &\subseteq R.
\end{align}

Finally, division can combine with the function composition in useful ways.  Let $f$ be a function, then the shunting rules are:\index{shunting laws!for division}
\begin{align}
  R \slash S \cdot f &= R \slash (f^\circ \cdot S) \label{law:div:shunting1} \\
  f \cdot R \slash S &= (f \cdot R) \slash S. \\
  f^\circ \cdot S \backslash R &= (S \cdot f) \backslash R \\
  S \backslash R \cdot f^\circ &= S \backslash (R \cdot f^\circ)
\end{align}
These are consequences of the definition and can be proved simply by drawing the appropriate diagram.

\subsection{Power transpose}
\label{sec:power-transpose}

The \emph{power transpose} operator enables us to transform a relation into a function by removing the indeterminism inherent in the definition of relation.

\begin{definition}\index{power transpose}
Let $R : A \leftarrow B$ be a relation.  Then the \emph{power transpose} of $R$ is the function $\Lambda R : \PF A \leftarrow B$ defined as
\[
(\Lambda R)\, x = \{ y \st yRx \}.
\]
The power transpose is characterized by the universal property\index{universal property!of power transpose}
\begin{align}
f = \Lambda R ~ \equiv ~\, \in \cdot \ f = R. \label{up:power-transpose}
\end{align}
\end{definition}

In words, the function $\Lambda R$ applied to $x$ returns all the values that are related to $x$ via $R$.  For example $(\Lambda leq)\, n$ would return the set of all numbers less than or equal to $n$.

From the universal property we can directly derive the \emph{cancellation} and \emph{reflection} laws for power transpose:\index{cancellation law!for power transpose}\index{reflection law!for power transpose}
\begin{align}
\in \cdot \ \Lambda R &= R \label{law:pt:cancel} \\
\Lambda\!\! \in &= id
\end{align}

Using the cancellation law, we get the fusion law for functions\index{fusion law!for power transpose}
\begin{align}
\Lambda(R \cdot f) = \Lambda R \cdot f. \label{law:pt:fusion}
\end{align}

The following lemma introduces another useful cancellation law.

\begin{lemma}\label{lem:pt:cancel}
  Let $R$ be a relation.  Then $\Lambda R \cdot R^\circ \subseteq \ \ni$.
\end{lemma}

\begin{proof}
The proof is a simple application of shunting law~\eqref{law:fun:shunting1} and monotonicity of converse:
\[
\Lambda R \cdot R^\circ \subseteq \ \ni \ \Leftrightarrow
R^\circ \subseteq (\Lambda R)^\circ \, \cdot \in^\circ \ \Leftrightarrow
R \subseteq \ \in \cdot \ \Lambda R \overset{\eqref{law:pt:cancel}} \Leftrightarrow
true.
\]
\qed
\end{proof}

Recall from section~\ref{sec:categories} the existential image functor~\eqref{def:ex-image}.  We can now define it in terms of power transpose by the equation
\begin{align}
  \EF R = \Lambda (R \ \cdot \in). \index{existential image}\label{def:ex-image-with-pt}
\end{align}
The close relation between existential image and power transpose is exemplified by the following fusion law:
\begin{align}
  \EF R \cdot \Lambda S = \Lambda (R \cdot S). \index{fusion law!for existential image}
\end{align}
The proof uses the universal property of $\Lambda$, the definition of $\EF$ and the cancellation law~\eqref{law:pt:cancel}.  We will argue on a line:
\[
\EF R \cdot \Lambda S = \Lambda (R \cdot S) \overset{\eqref{up:power-transpose}}\Leftrightarrow
\ \in \cdot \ \EF R \cdot \Lambda S = R \cdot S \overset{\eqref{def:ex-image-with-pt}}\Leftrightarrow
R \ \cdot \in \cdot \ \Lambda S = R \cdot S.
\]

This proof also highlights the fact that $\in \ : id \leftarrow \EF$ is a natural transformation, namely it satisfies the following property:
\begin{align}
  R \ \cdot \in \ = \ \in \cdot \ \Lambda (R \ \cdot \in) = \ \in \cdot \ \EF R \label{law:ex-image:natural}.
\end{align}

Lastly, we will introduce the singleton function\index{singleton set} $\tau : \PF A \leftarrow A$ that is defined by $\tau = \Lambda id$ or alternatively $\tau\, a = \{a\}$.  Using the fusion law~\eqref{law:pt:fusion} we obtain
\begin{align}
  \Lambda f = \Lambda (id \cdot f) = \Lambda id \cdot f = \tau \cdot f. \label{law:pt:overfun}
\end{align}
Recalling the definition of power functor~\eqref{def:powerfunctor} we get
\begin{align}
  \PF f \cdot \tau = \EF f \cdot \Lambda id = \Lambda (f \cdot id) = \Lambda f = \tau \cdot f
\end{align}
and so $\tau : \PF \leftarrow id$ is another example of natural transformation.

\subsection{Products and coproducts}
\label{sec:products-coproducts}

When working with composite types made of products and coproducts as outlined in definition~\ref{def:polyfunctors} it will be useful to have operators that can process these structures ``naturally''.  In category theory, product and coproduct (bi)functors are used to capture the idea.  Since we limit ourselves to the category $\REL$ we will use the standard relational product and coproduct.

We will denote the projection functions\index{projection function} from \emph{pairs} as $outl : A \leftarrow A \times B$ and $outr : B \leftarrow A \times B,$ and injection functions\index{injection function} into \emph{disjoint sums} as $inl : A + B \leftarrow A$ and $inr : A + B \leftarrow B$.  The semantics of pairs are those of the usual cartesian product.

The less known disjoint sum can be thought of as a tagged union.  Elements injected with $inl$ will have tag ``left'' and those injected with $inr$ will have tag ``right''.  For example, $inl\ 4$ would mean $(4, \text{left})$.

\begin{definition}\label{def:split}\index{split operator}
  Let $R : A \leftarrow C$ and $S : B \leftarrow C$ be relations.  Then the \emph{split} of $R$ and $S$ is denoted $\spl{R}{S}  : A \times B \leftarrow C$ and is defined as
\[
\spl{R}{S}  = (outl^\circ \cdot R) \cap (outr^\circ \cdot S).
\]
\end{definition}

We can interpret this in set theory as $(a,b)\, \spl{R}{S}\, c \Leftrightarrow aRc \wedge bSc$.  Using the definition~\ref{def:split} we can define the \emph{cross}\index{cross operator} operator $\times$ as
\begin{align}
R \times S = \spl{R \cdot outl}{S \cdot outr}, \label{def:cross}
\end{align}
or $(a,b)\, (R \times S)\,(c,d) \Leftrightarrow aRc \wedge bSd$.  Since the product is a bifunctor, we can compose two cross operators in the natural way:
\begin{align}
(R \times S) \cdot (U \times V) = (R \cdot U) \times (S \cdot V). \index{composition!of product}
\end{align}
A cross operation can also be fused into the split, as shown by the following absorption property:
\begin{align}
(R \times S) \cdot \spl{U}{V} = \spl{R \cdot U}{S \cdot V}. \label{def:fusion:split}\index{fusion law!for product}
\end{align}

Two useful properties are the \emph{naturality} properties of $outl$ and $outr$ with respect to the cross operator:
\begin{align}
  outl \cdot (R \times S) &\subseteq R \cdot outl \label{law:naturality-outr} \\
  outr \cdot (R \times S) &\subseteq S \cdot outr.
\end{align}
However, it should be made clear that they are not natural transformations as discussed in section \ref{sec:natur-transf}.  As a counter example, consider $outl \cdot (R \times \emptyset) \not = R \cdot outl$.

A similar set of properties follows for the coproduct.  The operation corresponding to the split is \emph{case}.

\begin{definition}\index{case operator}\label{def:case}
 Let $R : A \leftarrow B$ and $S : A \leftarrow C$ be relations.  Then the \emph{case} of $R$ and $S$ is denoted $[R, S] : A \leftarrow B + C$ and is defined as
\[
[R, S]  = (R \cdot inl^\circ) \cup (S \cdot inr^\circ).
\]
\end{definition}

The \emph{plus} operator is defined using case as
\begin{align}
  R + S = [inl \cdot R, inr \cdot S] \index{plus operator}
\end{align}
with composition and fusion laws similar to those of products:
\begin{align}
  (R + S) \cdot (U + V) &= R \cdot U + S \cdot V \index{composition!of coproduct}\\
  [R, S] \cdot (U + V) &= [R \cdot U, S \cdot V]. \index{fusion law!for coproduct}
\end{align}

Power transpose operator distributes over case and join in the following way.  Function $cup$ is binary union.
\begin{align}
  \Lambda [R, S] &= [\Lambda R, \Lambda S]  \label{law:pt:overcase} \\
  \Lambda (R \cup S) &= cup \cdot \spl{\Lambda R}{\Lambda S} \label{law:pt:overjoin}
\end{align}

\subsection{Minimum and maximum}
\label{sec:minimum-maximum}

The final piece of the theory is the definition of the minimum relation. This will be a key component of the chapters about optimization problems.

\begin{definition}\label{def:minimum}\index{minimum}
For any relation $R : A \leftarrow A$, not necessarily a preorder, the relation $min\, R : A \leftarrow \PF A$ is defined as
\[
min\, R = \ \in \cap \, (R \slash\!\!\ni).
\]
\end{definition}

The minimum relation satisfies the following universal property which follows directly from the definition:\index{universal property!of min@of \textsl{min}}
\begin{align}
\forall X : A \leftarrow \PF A ~.~ (X \subseteq min\, R ~~ \equiv ~~ X \subseteq \ \in \text{ and } X \, \cdot \ni \ \subseteq R). \label{up:minimum}
\end{align}

\begin{definition}\index{maximum}
For any relation $R : A \leftarrow A$, not necessarily a preorder,  the relation $max\, R : A \leftarrow \PF A$ is defined as
\[
max\, R = min\, R^\circ.
\]
\end{definition}

The following lemma generalizes the universal property from definition \ref{def:minimum}.  Indeed, if we let $S = \ \in$ we get \eqref{up:minimum}.
\begin{lemma}\label{lemma:min-powertranspose}
Let $R : A \leftarrow A,\, S : A \leftarrow B$ be relations.  Then
\[
min\, R \cdot \Lambda S = S \cap (R \slash S^\circ).
\]
\end{lemma}

\begin{proof}
\begin{derivation}
& min\, R \cdot \Lambda S &\\
= & \dtext{definition \ref{def:minimum} of $min$} &\\
& (\in \cap \, (R \slash\!\!\ni)) \cdot \Lambda S &\\
= & \dtext{$\Lambda S$ function} &\\
& \in \cdot \ \Lambda S \cap \, (R \slash\!\!\ni) \cdot \Lambda S &\\
= & \dtext{division \eqref{law:div:shunting1}, power transpose cancellation \eqref{law:pt:cancel}} &\\
& S \cap (R \slash S^\circ) &
\end{derivation}
\qed
\end{proof}

The result of lemma \ref{lemma:min-powertranspose} can therefore also be expressed as the universal property
\[
X \subseteq min\, R \cdot \Lambda S ~~ \equiv ~~ X \subseteq S \text{ and } X \cdot S^\circ \subseteq R.
\]

\begin{corollary}\label{lemma:min-eximage}
Because $\EF S = \Lambda (S \ \cdot \in)$ we get
\[
min\, R \cdot \EF S = (S \ \cdot \in) \cap (R \slash (S \ \cdot \in)^\circ).
\]
\end{corollary}

\index{relation|)}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% reftex-default-bibliography: ("./bibliography")
%%% End: